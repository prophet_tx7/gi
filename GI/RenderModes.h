#pragma once

#include "glm.hpp"

#include "Light.h"

using glm::mat4;
using glm::mat3;


/**
 * RenderingPipeline mode interface for lighting
 */
class Renderer::RMILight {

public:

    virtual ~RMILight() {}

    virtual void setLight(
        const mat4& viewMatrix,
        const Light& light
    ) = 0;
    
    virtual void setShadowMatrix(
        const mat4& shadowMatrix
    ) = 0;

    virtual void setShadowMap(
        const ShadowMap& shadowMap    
    ) = 0;

};


/**
 * RenderingPipeline mode interface for basic transformations
 */
class Renderer::RMITransform {

public:

    virtual ~RMITransform() {}

    virtual void setMatrices(
        const mat4& modelViewMatrix,
        const mat3& normalMatrix,
        const mat4& mvp
    ) = 0;

};


/**
 * Render mode interface for material setting
 */
class Renderer::RMIMaterial {

public:

	virtual ~RMIMaterial() {}

	virtual void setMaterial(
        const Material& material,
        const Texture* diffuse = nullptr,
        const Texture* specular = nullptr,
        const bool     setSpecular = true
    ) = 0;

};




/**
* Abstract render mode
*/
class Renderer::RenderMode {

public:

	virtual ~RenderMode() {}

	virtual void use() = 0;

};


/**
* Abstract rendering mode for RenderBuffers debugging
*/
class Renderer::RMBufferDebug : public RenderMode {

public:

    virtual void setBufferID(uint bufferID) = 0;

};


/**
 * Forward rendering mode
 */
class Renderer::RMForward : public RenderMode {

public:

    virtual RMITransform&   transformInterface() = 0;
    virtual RMILight&       lightInterface() = 0;
	virtual RMIMaterial&    materialInterface() = 0;
    virtual void            enableAmbient(bool enable) = 0;
};


/**
 * Shadow map creation rendering mode
 */
class Renderer::RMShadowMap : public RenderMode {

public:

    virtual void setMVP(const mat4& mvp) = 0;

};


/**
 * Reflective shadow map creation rendering mode
 */
class Renderer::RMRSM : public RenderMode {

public:

    virtual void setCameraViewMatrix(const mat4& viewMatrix) = 0;

	virtual RMIMaterial& materialInterface() = 0;

    /**
    * @pre setCameraViewMatrix() was called
    */
    virtual void setLight(const Light& light) = 0;

    /**
     * @pre setCameraViewMatrix() and setLight() was called
     */
    virtual void setModelMatrix(const mat4& modelMatrix) = 0;

};


/**
 * G-Buffer creation rendering mode
 */
class Renderer::RMGBuffer : public RenderMode {

public:

    virtual RMITransform&   transformInterface() = 0;
	virtual RMIMaterial&	materialInterface() = 0;

};


/**
 * Deferred shading from G-Buffer
 */
class Renderer::RMDeferred : public RenderMode {
    
public:

    explicit RMDeferred(const GBuffer& gbuffer) : m_gbuffer(gbuffer) {}
    
    /**
     * @param enable no color will be computed and written iff false
     */
    virtual void enableDirectLighting(bool enable) = 0;

    /**
     * Set reflective shadow map to use for indirect lighting
     * @param rsm   null for no inderect lighting
     */
    virtual void setRSM(const RSM* rsm) = 0;

    /**
     * Enables/disables brute-force indirect lighting
     */
    virtual void enableIndirectLighting(bool diffuse, bool specular) = 0;
    
    virtual RMILight& lightInterface() = 0;

protected:

    const GBuffer&  m_gbuffer;

};


/**
 * Shading buffer creation mode
 */
class Renderer::RMProbes : public RenderMode {

public:

    RMProbes(const RSM& rsm) : m_rsm(rsm) {}

    /**
     * @enable true iff the specular VPLs shall be computed
     */
    virtual void enableSpecular(bool enable) = 0;

    /**
     * Sets number of pixels in one dimension of ShadingBuffers being created.
     * Total number of pixels (ShadingBuffers capacity) will be dim*dim.
     */
    virtual void setShadingBufferDim(uint dim) = 0;

    virtual void setCameraViewMatrix(const mat4& viewMatrix) = 0;

    /**
     * @pre setCameraViewMatrix() was called
     */
    virtual void setLight(const Light& light) = 0;

    /**
     * How many probes have already been rendered?
     */
    virtual void setFirstProbeID(uint firstProbeID) = 0;

    /**
     * Sets matrices for probe transformation.
     */
    virtual void setMatrices(const mat4& modelViewMatrix, const mat3& normalMatrix) = 0;

    virtual void setMaterialShininess(const float shininess) = 0;

protected:

    const RSM& m_rsm;

};


/**
 * Abstract rendering mode for indirect illumination using shading buffers
 */
class Renderer::RMIndirect : public RenderMode {

public:

    explicit RMIndirect(const ShadingBuffers& shadingBuffers)
        : m_shadingBuffers(shadingBuffers) {}
    
    /**
    * @enable true iff the indirect specular light shall be rendered
    */
    virtual void enableSpecular(bool enable) = 0;

    virtual RMITransform&   transformInterface() = 0;
    virtual RMIMaterial&    materialInterface() = 0;

    /**
     * How many probes have already been rendered?
     */
    virtual void            setFirstProbeID(uint firstProbeId) = 0;

protected:

    const ShadingBuffers& m_shadingBuffers;

};
