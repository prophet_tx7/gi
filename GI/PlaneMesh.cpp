#include "PlaneMesh.h"

using glm::vec4;


PlaneMesh::PlaneMesh(float dimX, float dimZ, uint tesX, uint tesZ) {
    m_numVertices = (tesX + 1)*(tesZ + 1);
    m_numIndices = tesX*tesZ * 6;
    m_vertices = vertices_t(new VertexPNT[m_numVertices]);
    m_indices = indices_t(new uint[m_numIndices]);
	createPlaneMesh(
        dimX, dimZ, tesX, tesZ,
        m_vertices.get(), m_indices.get(), 0, mat4()
    );

}


void PlaneMesh::createPlaneMesh(
    float dimX, float dimY,
    uint tesX, uint tesY,
    VertexPNT* vertices,
    uint* indices,
    uint iFirstVertex,
    const mat4& transform
) {

    const float startY = dimY / 2.0f;
    const float stepX = dimX / tesX;
    const float stepY = dimY / tesY;
    const float texStepU = 1.f / tesX;
    const float texStepV = 1.f / tesY;
    float x = -dimX / 2.0f;
    
    for (uint i = 0; i < tesX + 1; i++) {
        float y = startY;
        for (uint j = 0; j < tesY + 1; j++) {
            VertexPNT& vertex = vertices[i*(tesY+1)+j];
            vertex.position = vec3((transform*vec4(x, y, 0.f, 1.0f)));
            vertex.normal = glm::normalize(vec3((transform*vec4(0, 0, 1.f, 1.0f))));
            vertex.texCoords = vec2(i*texStepU, 1.f-j*texStepV);
            y -= stepY;
            if (i < tesX && j < tesY)
                indexQuad(i, j, tesX, tesY, indices, iFirstVertex);
        }
        x += stepX;
    }


}

void PlaneMesh::createPlaneProbes(
    float dimX, float dimY,
    uint numProbesX, uint numProbesY,
    VertexPN* probes,
    const mat4& transform
) {

    //dimX -= dimX / numProbesX; // do not feed corners
    //dimY -= dimY / numProbesY; // dtto
    //dimY *= 0.9f;

    const float startY = dimY / 2.0f;
    const float stepX = dimX / (numProbesX-1);
    const float stepY = dimY / (numProbesY-1);
    float x = -dimX / 2.0f;

    for (uint i = 0; i < numProbesX; i++) {
        float y = startY;
        for (uint j = 0; j < numProbesY; j++) {
            VertexPN& vertex = probes[i*(numProbesY)+j];
            vertex.position = vec3((transform*vec4(x, y, 0.f, 1.0f)));
            vertex.normal = glm::normalize(vec3((transform*vec4(0, 0, 1.f, 1.0f))));
            y -= stepY;
        }
        x += stepX;
    }

}
