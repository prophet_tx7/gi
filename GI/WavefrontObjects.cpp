#pragma once

#include "WavefrontObjects.h"


#include <tiny_obj_loader.h>


using namespace std;



WavefrontObjects::WavefrontObjects(
    const char* basePath, const char* meshFileName
) {
    loadModels(basePath, meshFileName);
    createObjects();

}

WavefrontObjects::WavefrontObjects(
    const char* basePath, const char* meshFileName,
    const char* probesFileName, float numProbes
) {
    
    loadModels(basePath, meshFileName);
    loadProbes(basePath, probesFileName, numProbes);

    // assign light probes
    for (auto& probeSetPair : m_probes) {
        auto objIt = m_models.find(probeSetPair.first);
        if (objIt == m_models.end()) continue;
        for(auto& obj : objIt->second) obj->setLightProbes(*probeSetPair.second);
    }

    createObjects();

}

WavefrontObjects::~WavefrontObjects() {

    for (auto& modelPair: m_models) {
        for(auto& model : modelPair.second)   delete model;
    }

    for (auto& mesh: m_meshes) delete mesh;

    for (auto& object : m_objects) delete object;

    for (auto& probeSetPair : m_probes) delete probeSetPair.second;

}



void WavefrontObjects::loadModels(
    const char* basePath,
    const char* meshFileName
) {

    vector<tinyobj::shape_t>       shapes;
    vector<tinyobj::material_t>    materials;
    
    tinyobj::material_t mat;

    string completeFileName = basePath;
    completeFileName += meshFileName;

    auto err = tinyobj::LoadObj(shapes, materials, completeFileName.c_str(), basePath);

    m_meshes.reserve(shapes.size());
    
    for (auto& material : materials) {
        auto mtl = new Material(
            basePath + material.diffuse_texname,
            basePath + material.specular_texname
        );
        mtl->shininess = material.shininess;
        m_materials.push_back(mtl);
    }

    for (auto& shape : shapes) {

        auto numVertices = shape.mesh.positions.size();
        numVertices /= 3;
        Mesh::vertices_t vertices(new VertexPNT[numVertices]);
        for (uint i=0; i<numVertices; i++) {
            memcpy(&vertices[i].position, shape.mesh.positions.data()+i*3, 3*sizeof(float));
            memcpy(&vertices[i].normal, shape.mesh.normals.data()+i*3, 3*sizeof(float));
            memcpy(&vertices[i].texCoords, shape.mesh.texcoords.data()+i*2, 2*sizeof(float));
            vertices[i].texCoords.y = 1 - vertices[i].texCoords.y;
        }

        auto numIndices = shape.mesh.indices.size();
        Mesh::indices_t indices(new uint[numIndices]);
        memcpy(indices.get(), shape.mesh.indices.data(), sizeof(uint)*numIndices);

        auto mesh = new Mesh(move(vertices), numVertices, move(indices), numIndices);
        m_meshes.push_back(mesh);
        if (shape.mesh.material_ids[0]<0)   m_models[shape.name].push_back(new Model3D(*mesh));
        else {
            m_models[shape.name].push_back(new Model3D(
                *mesh,
                *m_materials[shape.mesh.material_ids[0]]
            ));
        }

    }

}


void WavefrontObjects::loadProbes(
    const char* basePath,
    const char* fileName,
    float probeSubsetSize
) {

    vector<tinyobj::shape_t>       shapes;
    vector<tinyobj::material_t>    materials;


    string completeFileName = basePath;
    completeFileName += fileName;
    auto err = tinyobj::LoadObj(shapes, materials, completeFileName.c_str(), basePath);

    for (auto& shape : shapes) {
        auto numProbes = shape.mesh.positions.size();
        numProbes /= 3;
        auto probeSet = new Model3D::ProbeSet(numProbes);
        auto pProbes = probeSet->probes.get();
        for (uint i=0; i<numProbes; i++) {
            memcpy(&pProbes[i].position, shape.mesh.positions.data()+i*3, 3*sizeof(float));
            memcpy(&pProbes[i].normal, shape.mesh.normals.data()+i*3, 3*sizeof(float));
        }
        probeSet->reduce(probeSubsetSize);
        m_probes.emplace(string(shape.name), probeSet);
    }

}

void WavefrontObjects::createObjects() {
    m_objects.reserve(m_models.size());
    for (auto& modelPair : m_models) {
        for(auto& model : modelPair.second) m_objects.push_back(new Object3D(*model));
    }
}
