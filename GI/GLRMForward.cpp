#include "GLRenderer.h"
#include "GLShader.h"

GLRenderer::GLRMForward::GLRMForward()
    : m_rmiTransform(m_program), m_rmiLight(m_program), m_rmiMaterial(m_program)
{

    GLShader vertex(GL_VERTEX_SHADER, "330", "FragmentLit VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "FragmentLit FragmentShader");

	vertex.addSource("#define ALL_ATTRIBUTES");
    vertex.addSource("#define SHADOW_COORDS");
    vertex.addSourceFromFile("shaders/vertex.vert");

    fragment.addSourceFromFile("shaders/lighting.glsl");
    fragment.addSourceFromFile("shaders/shading.glsl");
    fragment.addSourceFromFile("shaders/shadow_mapping.glsl");
    fragment.addSourceFromFile("shaders/fragment_lighting.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();

}

void GLRenderer::GLRMForward::use() {
    m_program.use();
}

Renderer::RMITransform& GLRenderer::GLRMForward::transformInterface() {
    return m_rmiTransform;
}

Renderer::RMILight& GLRenderer::GLRMForward::lightInterface() {
    return m_rmiLight;
}

void GLRenderer::GLRMForward::enableAmbient(bool enable) {
    m_program.setUniform("ambientLight", vec3( enable ? 0.045f : 0.f ));
}

Renderer::RMIMaterial& GLRenderer::GLRMForward::materialInterface() {
	return m_rmiMaterial;
}
