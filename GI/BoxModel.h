#pragma once

#include "Model3D.h"
#include "BoxMesh.h"

class BoxModel : public Model3D {
    
public:
    

    BoxModel(
        float sizeX, float sizeY, float sizeZ,
        uint tesX, uint tesY, uint tesZ,
        uint numProbesX, uint numProbesY, uint numProbesZ,
        const Material& material,
        float probeSubsetSize = 1.0
    ) :
        Model3D(m_mesh, material),
        m_mesh(sizeX, sizeY, sizeZ, tesX, tesY, tesZ)
    {
        createProbes(sizeX, sizeY, sizeZ, numProbesX, numProbesY, numProbesZ, probeSubsetSize);
    }
    

    BoxModel(
        float sizeX, float sizeY, float sizeZ,
        uint tesX, uint tesY, uint tesZ,
        uint numProbesX, uint numProbesY, uint numProbesZ,
        float probeSubsetSize = 1.0
    ) :
        Model3D(m_mesh),
        m_mesh(sizeX, sizeY, sizeZ, tesX, tesY, tesZ)
    {
        createProbes(sizeX, sizeY, sizeZ, numProbesX, numProbesY, numProbesZ, probeSubsetSize);
    }


    BoxModel(
        float size, uint tes, uint numProbes, const Material& material,
        float probeSubsetSize = 1.0
    ) : BoxModel(
        size, size, size,
        tes, tes, tes,
        numProbes, numProbes, numProbes,
        material,
        probeSubsetSize
    ) {}


    BoxModel(
        float size, uint tes, uint numProbes,
        float probeSubsetSize = 1.0
    ) : BoxModel(
        size, size, size,
        tes, tes, tes,
        numProbes, numProbes, numProbes,
        probeSubsetSize
    ) {}

    ~BoxModel() {
        delete m_probeSet;
    }

private:

    BoxMesh     m_mesh;
    ProbeSet*   m_probeSet;


    void createProbes(
        float sizeX, float sizeY, float sizeZ,
        uint numProbesX, uint numProbesY, uint numProbesZ,
        float probeSubsetSize = 1.0
    ) {
        
        auto numProbes
            = 2 * numProbesX * numProbesY
            + 2 * numProbesX * numProbesZ
            + 2 * numProbesY * numProbesZ;

        m_probeSet = new ProbeSet(numProbes);

        createProbeVertices(
            m_probeSet->probes.get(),
            sizeX, sizeY, sizeZ, numProbesX, numProbesY, numProbesZ
        );

        m_probeSet->reduce(probeSubsetSize);

        setLightProbes(*m_probeSet);

    }


    void createProbeVertices(
        VertexPN* probes,
        float sizeX, float sizeY, float sizeZ,
        uint numProbesX, uint numProbesY, uint numProbesZ
    ) {
        
        // generate 6 planes
        mat4 transform;
        uint iProbe = 0;
        //XZ
        transform = translate(vec3(0, sizeY / 2.f, 0));
        transform = rotate(transform, -half_pi<float>(), vec3(1.f, 0.f, 0.f));
        createOpositePlanes(
            probes,
            sizeX, sizeZ, numProbesX, numProbesZ,
            transform, vec3(1, 0, 0),
            iProbe
            );
        //XY
        transform = translate(vec3(0, 0, sizeZ / 2.f));
        createOpositePlanes(
            probes,
            sizeX, sizeY, numProbesX, numProbesY,
            transform, vec3(1, 0, 0),
            iProbe
            );
        //YZ
        transform = translate(vec3(sizeX / 2.f, 0, 0));
        transform = rotate(transform, half_pi<float>(), vec3(0, 1, 0));
        createOpositePlanes(
            probes,
            sizeZ, sizeY, numProbesZ, numProbesY,
            transform, vec3(0, 0, 1),
            iProbe
            );
        
    }

    void createOpositePlanes(
        VertexPN* probes,
        float sizeX, float sizeY, uint numProbesX, uint numProbesY,
        const mat4& transform, const vec3&  rotationAxis,
        uint& iProbe
    ) {

        int numProbes = numProbesX*numProbesY;

        PlaneMesh::createPlaneProbes(
            sizeX, sizeY, numProbesX, numProbesY,
            probes + iProbe,
            transform
            );

        PlaneMesh::createPlaneProbes(
            sizeX, sizeY, numProbesX, numProbesY,
            probes + iProbe + numProbes,
            rotate(pi<float>(), rotationAxis)*transform
            );

        iProbe += 2 * numProbes;
    }
    
};
