#pragma once

#include "Model3D.h"
#include "Transform.h"

/**
 * 3D object representation that an Renderer class can render
 */
class Object3D{

public:

    explicit Object3D(const Model3D& model):
        m_model(model) {}

    const Model3D& getModel() const { return m_model; }

	Transform& getTransform() { return m_transform; }
	const Transform& getTransform() const { return m_transform; }

private:

    const Model3D& m_model;
	Transform m_transform;

};
