#include "GLRenderer.h"

#include "SDL.h"

#include "CubeScene.h"
#include "SponzaScene.h"

#include <iostream>


using namespace std;



#define HR "==============================================================================="



/**
 * Various user-specifiable application parameters;
 */
struct Parameters {

    enum SceneType { CUBES, SPONZA, SPONZA_DAY };
    
    SceneType   scene       = SPONZA;
    uint        scrWidth    = 1280;
    uint        scrHeight   =  720;
    bool        fullscreen  = false;
    
    /**
     * % of light probes to create, higher is nicer lower yields in better performance
     */
    float       numProbes   = 5.f;
    
    /**
     * Reflective shadow map (for indirect light) resolution (in one dimension)
     */
    uint        rsmDim      = 32;

    /**
     * Shadow map resolution (in one dimension)
     */
    uint        smDim       = 2048;

};


/**
 * Enumeration for printSDLError() function
 */
enum ErrorType {FATAL_ERROR, WARNING};


/**
 * Prints command-line-arugments description
 */
void printUsageMessage() {
    cout    << "Usage: <program> ";
    cout    << "[--cubes|sponza|sponzaDay] ";
    cout    << "[--res <w>x<h>] ";
    cout    << "[--fullscreen] ";
    cout    << "[--numProbes <%>] ";
    cout    << "[--rsmDim <x>] ";
    cout    << "[--smDim <x>] ";
    cout    << endl << endl;
    cout    << "where <%> is a number in (0,100> range, ";
    cout    << "<w>, <h> is window/screen width, height respectively ";
    cout    << "and <x> is integer.";
    cout    << endl << endl;
    cout    << "e.g.: gi.exe --sponzaDay --res 1920x1080 --fullscreen --numProbes 75 "
            << "--rsmDim 64 --smDim 4096";
    cout    << endl << endl;
}


/**
 * Parses command line arguments and fills Parameters out variable.
 * @return true iff the arguments are OK.
 */
inline bool parseInputParameters(int argc, char* argv[], Parameters& out) {

    if (argc==1) return true;

    for (int i=1; i<argc; i++) {

        if(argv[i] == string("--numProbes")) {
            if (++i>=argc) return false;
            float numProbes = float(atof(argv[i]));
            if (numProbes <= 0.f || numProbes > 100.f) return false;
            out.numProbes = numProbes;
  
        } else if (argv[i] == string("--cubes")) {
            out.scene = Parameters::CUBES;

        } else if (argv[i] == string("--sponza")) {
            out.scene = Parameters::SPONZA;

        } else if (argv[i] == string("--sponzaDay")) {
            out.scene = Parameters::SPONZA_DAY;

        } else if (argv[i] == string("--res")) {
            if (++i>=argc) return false;
            string arg = argv[i];
            auto x = arg.find("x");
            if (x==string::npos) return false;
            int width = atoi(arg.substr(0, x).c_str());
            int height = atoi(arg.substr(x+1).c_str());
            if (width==0 || height==0) return false;
            out.scrWidth = width; out.scrHeight = height;

        }  else if (argv[i] == string("--fullscreen")) {
            out.fullscreen = true;

        } else if (argv[i] == string("--rsmDim")) {
            if (++i>=argc) return false;
            int rsmDim = atoi(argv[i]);
            if (rsmDim<=0) return false;
            out.rsmDim = rsmDim;

        } else if (argv[i] == string("--smDim")) {
            if (++i>=argc) return false;
            int smDim = atoi(argv[i]);
            if (smDim<=0) return false;
            out.smDim = smDim;

        } else {
            return false;

        }
    
    }

    return true;

}


/**
 * Prints all parameter names and their settings
 */
inline void printParameters(Parameters p) {
    cout << "Parameters set: " << endl;
    cout << "---------------"  << endl << endl;
    cout << "Scene:\t\t\t\t\t\t\t";
    switch(p.scene) {
        case Parameters::CUBES:         cout << "Cubes"; break;
        case Parameters::SPONZA:        cout << "Sponza"; break;
        case Parameters::SPONZA_DAY:    cout << "Sponza (\"daylight\")"; break;
    }
    cout << endl;
    cout << "Screen resolution:\t\t\t\t\t" << p.scrWidth << "x" << p.scrHeight << endl;
    cout << "Fullscreen:\t\t\t\t\t\t" << (p.fullscreen ? "Yes" : "No") << endl;
    cout << "Shadow map resolution:\t\t\t\t\t" << p.smDim <<  "x" << p.smDim  <<endl;
    cout << "Reflective shadow map resolution:\t\t\t" << p.rsmDim <<  "x" << p.rsmDim  <<endl;
    cout << "Sparse shading points (light probes) subset size:\t" << p.numProbes << " %" << endl;
    cout << endl << HR << endl << endl;
}


/**
 * Prints nicely last SDL error to the standard output
 */
void printSDLError(const char msg[], ErrorType type){
	std::cerr << (type == FATAL_ERROR ? "Fatal error" : "Warning")
		 << ": " << msg << " SDL error: " << SDL_GetError() << "\n";
}


/**
 * Initializes SDL with window and OpenGL context.
 * Turns V-Sync on.
 */
inline bool initSDL(SDL_Window** window, int w, int h, bool fullscreen){

	// SDL init

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printSDLError("Could not init SDL video!", FATAL_ERROR);
		return false;
	}

	*window = NULL;

	*window = SDL_CreateWindow(
		"Global Illumination",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		w, h,
		SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0)
	);
	
	if (window == NULL) {
		printSDLError("Window could not be created!", FATAL_ERROR);
		return false;
	}

	// SDL OpenGL init

	SDL_GLContext context = NULL;

    SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	context = SDL_GL_CreateContext(*window);
	
	if (context == NULL) {
		printSDLError("OpenGL context could not be created!", FATAL_ERROR);
		return false;
	}

	if (SDL_GL_SetSwapInterval(0) < 0) {
		printSDLError("Warning: Unable to set VSync!", WARNING);
	}

	return true;

}


/**
 * Prints human readable description of given rendering pipeline.
 * Clears rest of the line possibly printed previously by this method
 * and performs carriage return.
 */
void printRenderingPipeline(Renderer::RenderingPipeline pipeline) {
    if (pipeline & Renderer::FORWARD)   cout << "Forward - direct ";
    else                                cout << "Deferred - ";
    if (pipeline & Renderer::_DIRECT)   cout << "direct ";
    if (pipeline & Renderer::_AMBIENT)  cout << "+ ambient ";
    if (pipeline & Renderer::_GI_DIFF || pipeline & Renderer::_GI_SPEC) {
        if (pipeline & Renderer::_DIRECT) cout << "+ ";
        if (pipeline & Renderer::_BRUTEFORCE)
                                        cout << "bruteforce ";
        else                            cout << "LightSkin ";
        cout << "indirect ";
    }
    if (pipeline & Renderer::_GI_DIFF)  cout << "diffuse ";
    if (pipeline & Renderer::_GI_SPEC)  cout << "and specular ";
    cout << "light";

    // clear potential previously printed rest of the line
    auto numSpaceBlocks = 4;
    if (pipeline & Renderer::_AMBIENT)  numSpaceBlocks--;
    if (pipeline & Renderer::_GI_DIFF)  numSpaceBlocks--;
    if (pipeline & Renderer::_GI_SPEC)  numSpaceBlocks--;
    if (pipeline & (Renderer::_GI_DIFF |  Renderer::_GI_SPEC))  numSpaceBlocks--;
    for (auto i=0; i<numSpaceBlocks*11; i++) cout << " ";
    
    cout << "\r";
}


/**
 * Prints application controls description
 */
void printControls(bool cameraControls) {

    cout << "Controls:" << endl;
    cout << "---------" << endl << endl;

    if (cameraControls) {
        cout << "WASD or UP/LEFT/DOWN/RIGHT for camera translation (Sponza only)" << endl;
        cout << "Mouse movement for camera rotation (Sponza only)" << endl;
        cout << "ESC    for mouse release (Sponza only)" << endl;
        cout << endl;
    }

    cout << "F1     for forward diffuse+specular shading" << endl;
    cout << "F2     for forward diffuse+specular+ambient shading" << endl;
    cout << "F3     for deferred diffuse+specular shading" << endl;
    cout << "F4     for deferred indirect diffuse shading" << endl;
    cout << "F5     for deferred indirect diffuse+specular shading" << endl;
    cout << "F6     for deferred diffuse+specular + indirect diffuse shading" << endl;
    cout << "F7-F12 for deferred diffuse+specular + indirect diffuse+specular shading" << endl;
    cout << "TAB    for LightSkin / Bruteforce indirect lighting switching" << endl << endl;

}


/**
 * Processes given SDL_KeyboardEvent key-up event.
 */
inline void onKeyUp(const SDL_KeyboardEvent& key, Renderer* renderer, bool& bruteforceGI){

    switch (key.keysym.scancode) {

        case SDL_SCANCODE_ESCAPE:
            // release mouse
            SDL_SetRelativeMouseMode(SDL_FALSE);
            break;

        case SDL_SCANCODE_TAB:
            // toggle bruteforce GI / LightSkin
            bruteforceGI = !bruteforceGI;
            // a little hack:
            {
                auto pipeline = renderer->getPipeline();
                if (bruteforceGI) {
                    if (pipeline & (Renderer::_GI_SPEC|Renderer::_GI_DIFF)) {
                        renderer->setPipeline(static_cast<Renderer::RenderingPipeline>(
                            pipeline | Renderer::_BRUTEFORCE
                            ));
                    }
                }
                else {
                    renderer->setPipeline(static_cast<Renderer::RenderingPipeline>(
                        pipeline & ~Renderer::_BRUTEFORCE
                        ));
                }
                break;
            }

        case SDL_SCANCODE_F1:
            renderer->setPipeline(Renderer::FORWARD);
            break;

        case SDL_SCANCODE_F2:
            renderer->setPipeline(Renderer::FORWARD_AMBIENT);
            break;

        case SDL_SCANCODE_F3:
            renderer->setPipeline(Renderer::DEFERRED_DIRECT);
            break;

        case SDL_SCANCODE_F4:
            renderer->setPipeline(
                bruteforceGI
                    ? Renderer::DEFERRED_BF_GI_DIFF
                    : Renderer::DEFERRED_GI_DIFF
            );
            break;

        case SDL_SCANCODE_F5:
            renderer->setPipeline(
                bruteforceGI
                    ? Renderer::DEFERRED_BF_GI_FULL
                    : Renderer::DEFERRED_GI_FULL
                );
            break;

        case SDL_SCANCODE_F6:
            renderer->setPipeline(
                bruteforceGI
                    ? Renderer::DEFERRED_DIRECT_BF_GI_DIFF
                    : Renderer::DEFERRED_DIRECT_GI_DIFF
                );
            break;

        case SDL_SCANCODE_F7:
        case SDL_SCANCODE_F8:
        case SDL_SCANCODE_F9:
        case SDL_SCANCODE_F10:
        case SDL_SCANCODE_F11:
        case SDL_SCANCODE_F12:
            renderer->setPipeline(bruteforceGI
                ? Renderer::DEFERRED_BF_FULL
                : Renderer::DEFERRED_FULL
            );
            break;

        default:
            break;

    }

    printRenderingPipeline(renderer->getPipeline());

}


/**
 * Application main processing loop
 */
inline void runMainLoop(
    Scene* scene, Renderer* renderer, SDL_Window* window,
    bool enableCameraControls
){

    if(enableCameraControls) SDL_SetRelativeMouseMode(SDL_TRUE);

    // run it!
    bool done = false;
    bool bruteforceGI = false;
    auto lastTicks = SDL_GetPerformanceCounter();
    SDL_Event evt;
    while (!done) {

        // handle SDL messages
        while (SDL_PollEvent(&evt)) {
            switch (evt.type) {
                
                case SDL_QUIT:
                    done = true;
                    break;
                
                case SDL_KEYUP:
                    onKeyUp(evt.key, renderer, bruteforceGI);
                    break;
                
                case SDL_MOUSEBUTTONDOWN:
                    if(enableCameraControls) SDL_SetRelativeMouseMode(SDL_TRUE);
                    break;
            }
        }

        // compute delta time
        auto ticks = SDL_GetPerformanceCounter();
        float deltaTime = (ticks-lastTicks)/double(SDL_GetPerformanceFrequency());
        lastTicks = ticks;

        // mouse controls in the sponza scene
        if (enableCameraControls && SDL_GetRelativeMouseMode()) {
        
            // mouse
            int mouseX, mouseY;
            SDL_GetRelativeMouseState(&mouseX, &mouseY);
            scene->getCamera().rotateY(mouseX*0.001f);

            // keyboard
            auto kbdState = SDL_GetKeyboardState(nullptr);
            if (kbdState[SDL_SCANCODE_UP] || kbdState[SDL_SCANCODE_W])
                scene->getCamera().translateForward(deltaTime*600);
            if (kbdState[SDL_SCANCODE_DOWN] || kbdState[SDL_SCANCODE_S])
                scene->getCamera().translateForward(-deltaTime*600);
            if (kbdState[SDL_SCANCODE_LEFT] || kbdState[SDL_SCANCODE_A])
                scene->getCamera().translateRight(-deltaTime*350);
            if (kbdState[SDL_SCANCODE_RIGHT] || kbdState[SDL_SCANCODE_D])
                scene->getCamera().translateRight(deltaTime*350);
        }

        // update the scene
        scene->update(deltaTime);

        // render
        renderer->render();
        SDL_GL_SwapWindow(window);

    }

}



/**
 * Application entry point.
 */
int main(int argc, char* argv[]) {

    Parameters parameters;

    if (argc<=1) {
        cout << "No arguments specified, using the defaults." << endl;
        printUsageMessage();
        cout << HR << endl << endl;
    } if (!parseInputParameters(argc, argv, parameters)) {
        cout    << "Invalid arguments." << endl;
        printUsageMessage();
        exit(EXIT_FAILURE);
    }

    printParameters(parameters);

    bool enableCameraControls = false;
    Renderer* renderer = NULL;
	SDL_Window* window = NULL;
	int returnCode = 0;
    bool done = false; // the main loop terminator
    Scene* scene = nullptr;

    // init SDL / OpenGL

    cout << "Initializing SDL ..." << endl;
 	if (!initSDL(&window, parameters.scrWidth, parameters.scrHeight, parameters.fullscreen)) {
		returnCode = 1;
		goto cleanup;
	}

    // create something to render

    cout << "Creating/loading the scene ..." << endl;
    float aspectRatio = (float)parameters.scrWidth / (float)parameters.scrHeight;
    switch(parameters.scene) {
        case Parameters::SceneType::CUBES:
            scene = new CubeScene(aspectRatio, parameters.numProbes/100.f);
            break;
        case Parameters::SceneType::SPONZA:
            scene = new SponzaScene<false>(aspectRatio, parameters.numProbes/100.f);
            enableCameraControls = true; // hide the cursor etc..
            break;
        case Parameters::SceneType::SPONZA_DAY:
            scene = new SponzaScene<true>(aspectRatio, parameters.numProbes/100.f);
            enableCameraControls = true; // hide the cursor etc..
            break;
    }

    // create a renderer

    cout << "Creating the OpenGL renderer ..." << endl;
    try{
        renderer = new GLRenderer(parameters.scrWidth, parameters.scrHeight);
        renderer->init(parameters.smDim, parameters.rsmDim);
    } catch(const RendererException& e){
        std::cerr << "FATAL ERROR - Renderer Exception: " << e.what() << "\n";
        if(e.details){
            std::cerr << "Details: " << e.details << "\n\n";
        }
        returnCode = EXIT_FAILURE;
        goto cleanup;
	}
	
    // fill the renderer

    cout << "Filling the renderer with the scene objects ..." << endl;
    for (auto object : scene->getObjects()) renderer->add3DObject(*object);
    for (auto light : scene->getLights()) renderer->addLight(*light);
    renderer->setCamera(scene->getCamera());

	// run it!

    cout << "Starting the application's main loop ..." << endl << endl;
    cout << HR << endl << endl;
    printControls(enableCameraControls);
    cout << "Current rendering pipeline: " << endl;
    cout << "---------------------------" << endl << endl;
    printRenderingPipeline(renderer->getPipeline());

    SDL_ShowWindow(window);
    runMainLoop(scene, renderer, window, enableCameraControls);

    cout << endl << endl;

cleanup:

    delete renderer;
    delete scene;
	if(window != NULL) SDL_DestroyWindow(window);
	SDL_Quit();	
	return returnCode;

}
