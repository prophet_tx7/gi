#pragma once

#include "renderer.h"

#include "gl_core_3_3.h"

#include "RendererExceptions.h"


/**
 * OpenGL implementation of abstract Renderer class
 */
class GLRenderer : public Renderer{

public:

    GLRenderer(uint width, uint height);


protected:

    // GL render modes creators

    virtual unique_ptr<RMBufferDebug>   createBufferDebugRM() override;
    virtual unique_ptr<RMForward>       createForwardRM() override;
    virtual unique_ptr<RMShadowMap>     createShadowMapRM() override;
    virtual unique_ptr<RMRSM>           createRSMRM() override;
    virtual unique_ptr<RMGBuffer>       createGBufferRM() override;
    virtual unique_ptr<RMDeferred>      createDeferredRM(const GBuffer& gbuffer) override;
    virtual unique_ptr<RMProbes>        createProbesRM(const RSM& rsm) override;
    virtual unique_ptr<RMIndirect>      createIndirectRM(
                                            const ShadingBuffers& shadingBuffers
                                        ) override;

    // GL render targets creators

    virtual unique_ptr<FrameBuffer>     createFrameBuffer() override;
    virtual unique_ptr<RenderBuffers>   createRenderBuffers(
                                            uint width, uint height
                                        ) override;

    // GL scene object wrappers

    virtual unique_ptr<RObject3D>       createRObject3D(
                                            const Object3D& object
                                        ) override;

    virtual unique_ptr<RLight>          createRLight(
                                            const Light& light,
                                            ShadowMap* shadowMap,
                                            RSM* rsm
                                        ) override;

    // graphics API abstraction

    virtual void clearBuffers(uint mask) override;

    virtual void enableDepthTest(const bool enable) override;

	virtual void setPolygonOffset(const float factor, const float units) override;

	virtual void enablePolygonOffset(const bool enable) override;

    virtual void setBlendingMode(BlendingMode mode) override;

    unique_ptr<Texture> createTexture(const void* data, uint width, uint height) override;

private:

    class GLObject3D;
    class GLLight;

    class GLTexture;

    class GLRenderBuffers;
    class GLFrameBuffer;

    class GLRMBufferDebug;
    class GLRMForward;
    class GLRMDeferred;
    class GLRMShadowMap;
    class GLRMRSM;
    class GLRMGBuffer;
    class GLRMDeferred;
    class GLRMProbes;
    class GLRMIndirect;

    class GLRMITransform;
    class GLRMILight;
	class GLRMIMaterial;

};


class GLRenderer::GLFrameBuffer : public FrameBuffer{

public:

    GLFrameBuffer(uint width, uint height) : FrameBuffer(width, height) {}
    
    void bind() override {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        resetRenderingViewPort();
    }

    void setRenderingViewPort(int x, int y, int width, int height, bool clear) override {
        glViewport(x, y, width, height); // TODO: throw if not bound
        if (clear) {
            glEnable(GL_SCISSOR_TEST);
            glScissor(x, y, width, height);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glDisable(GL_SCISSOR_TEST);
        }
    }

    void resetRenderingViewPort() override {
        glViewport(0, 0, m_width, m_height); // TODO: throw if not bound
    }

};


class GLRenderer::GLTexture : public Texture{
    
public:

    GLTexture(const void* data, uint width, uint height);
    ~GLTexture();

    void bind(GLenum texUnit) const;

private:

    GLuint  m_iTexture;

};


#include "GLSceneObjectWrappers.h"
#include "GLRMIs.h"
#include "GLRenderBuffers.h"
#include "GLRenderModes.h"
