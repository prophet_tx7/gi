/**
 * It just passes unmodified vertex position -> gl_Position
 * and vertex texture coordinate -> texCoord.
 */

layout (location=0) in vec3 vertexPosition;
layout (location=1) in vec3 vertexNormal;
layout (location=2) in vec2 vertexTexCoords;

out vec2 texCoords;

void main(){
    gl_Position = vec4(vertexPosition, 1.f);
    texCoords = vertexTexCoords;
}
