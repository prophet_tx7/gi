/**
 * G-Buffer creation shader
 */

in vec3 position;
in vec3 normal;
in vec2 texCoords;

layout (location = 1) out vec3 normalData;
layout (location = 2) out vec4 positionData;
layout (location = 3) out vec3 materialData;

void main(){
    positionData.xyz    = position;
    positionData.w      = gl_FragCoord.z;
    normalData          = normalize(normal);
    vec3 diff           = material.useDiffuseMap
                            ? texture2D(diffuseMap, texCoords).rgb
                            : material.diffuse;
    vec3 spec           = material.useSpecularMap
                            ? texture2D(specularMap, texCoords).rgb
                            : material.specular;
    materialData.x      = color2float(diff);
    materialData.y      = color2float(spec);
    materialData.z      = material.shininess;
    
}
