vec2 probeIdToUv(uint probeId, uint bufferDim) {
    uint y = probeId / bufferDim;
    uint x = probeId % bufferDim;
    return vec2(
        (x+0.5f)/float(bufferDim),
        1.f - (y+0.5f)/float(bufferDim)
    );
}

vec4 probeIdToClipSpace(uint probeId, uint bufferDim){
    uint y = probeId / bufferDim;
    uint x = probeId % bufferDim;
    return vec4(
        ((x+0.5f)/float(bufferDim)*2.f)-1.f,
        1.f-((y+0.5f)/float(bufferDim)*2.f),
        0, 1
    );
}
