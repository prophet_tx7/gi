in vec2 texCoords;

uniform sampler2D buffer;

out vec4 fragColor;

void main(){
    fragColor = texture2D(buffer, texCoords);    
}
