uniform sampler2D gbNormalTex;
uniform sampler2D gbPositionTex;
uniform sampler2D gbRgbTex;
uniform sampler2D rsmNormalTex;
uniform sampler2D rsmPositionTex;
uniform sampler2D rsmRgbTex;

uniform bool direct;
uniform bool indirectDiffuse;
uniform bool indirectSpecular;
uniform uint bufferDim;
uniform mat4 shadowMatrix;

in vec2 texCoords;

layout (location=0) out vec4 fragColor;

void main(){

    fragColor = vec4(0,0,0,1);

    vec4 pos_       = texture(gbPositionTex, texCoords);
    gl_FragDepth    = pos_.w + 5e-8; // write the depth (with some small offset)

    if(!direct && !indirectDiffuse && !indirectSpecular) return;

    vec3 material_  = texture( gbRgbTex, texCoords ).xyz;
    vec3 normal     = texture(gbNormalTex, texCoords).xyz;
    vec3 pos        = pos_.xyz;
    
    Material material;
    material.diffuse    = float2color(material_.x);
    material.specular   = float2color(material_.y);
    material.shininess  = material_.z;

    if(direct){
	    vec4 shadowCoords = shadowMatrix*vec4(pos,1.0);
        fragColor = vec4(
	    	diffuseSpecular(material, normal, pos) * (shadowCoef(shadowCoords)),
            1.0
	    );
    }

    if(indirectDiffuse || indirectSpecular){

        float rsmStep = 1.f/bufferDim;

        vec2 uv;
        vec3 v = normalize(-pos);
        for(uv.x=rsmStep/2.f; uv.x<=1.f; uv.x+=rsmStep){
            for(uv.y=rsmStep/2.f; uv.y<=1.f; uv.y+=rsmStep){
                
                vec3    valPos          = vec3( texture(rsmPositionTex, uv) );
                vec3    valNormal       = vec3( texture(rsmNormalTex, uv) );
                vec3    valFlux         = texture(rsmRgbTex, uv).rgb;
                
                float w;
                float wcos = diskToPoint(valPos, valNormal, pos, normal, rsmStep, w);
                if(indirectDiffuse)
                    fragColor.rgb += valFlux * material.diffuse * wcos;
                if(indirectSpecular){
                    vec3 s = normalize(valPos - pos);
                    fragColor.rgb
                        += valFlux * material.specular * w
                        * phongSpec(v, s, normal, material.shininess);
                }

            }
        }

    }

}
