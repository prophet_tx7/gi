/**
 * Shadow map sampler and shadowCoef() function
 */



uniform sampler2DShadow shadowMap;

/**
 * @return textureProj() result
 */
float shadowCoef(vec4 shadowCoord){
    return textureProj(shadowMap, shadowCoord);
}
