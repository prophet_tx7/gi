struct Material{
    vec3        diffuse;
    vec3        specular;
    float       shininess;
    bool        useDiffuseMap;
    bool        useSpecularMap;
};

uniform Material    material;
uniform sampler2D   diffuseMap;
uniform sampler2D   specularMap;




vec3 _diffuse(vec3 diffuseColor, vec3 s, vec3 normal);
vec3 _specular(vec3 specularColor, float shininess, vec3 v, vec3 s, vec3 normal);



/**
 * Diffuse shading
 */

vec3 diffuseEx(vec3 diffuseColor, vec3 normal, vec3 s, vec3 flux){
   return flux * _diffuse(diffuseColor, s, normal);
}

vec3 diffuse(vec3 diffuseColor, vec3 normal, vec3 position){
    vec3 s = dirToLight(position);
    vec3 flux =  lightIntensity(position, s);
	return diffuseEx(diffuseColor, normal, s, flux);
}

vec3 diffuse(Material material, vec3 normal, vec3 position){
    return diffuse(material.diffuse, normal, position);
}


vec3 diffuseEx(Material material, vec3 normal, vec3 s, vec3 flux){
    return diffuseEx(material.diffuse, normal, s, flux);
}

vec3 diffuse(vec3 normal, vec3 position){
    return diffuse(material, normal, position);
}

vec3 diffuseEx(vec3 normal, vec3 s, vec3 flux){
    return diffuseEx(material, normal, s, flux);
}

vec3 diffuse(vec2 uv, vec3 normal, vec3 position){
    if(!material.useDiffuseMap) return diffuse(normal, position);
    else {
        vec3 diffColor = texture2D(diffuseMap, uv).rgb;
        return diffuse(diffColor, normal, position);
    }
}

vec3 diffuseEx(vec2 uv, vec3 normal, vec3 s, vec3 flux){
    if(!material.useDiffuseMap) return diffuseEx(normal, s, flux);
    else {
        vec3 diffColor = texture2D(diffuseMap, uv).rgb;
        return diffuseEx(diffColor, normal, s, flux);
    }
}



/**
 * Phong shading using half-way vector
 */
vec3 diffuseSpecular(
    vec3 diffuseColor, vec3 specularColor, float shininess,
    vec3 normal, vec3 position
){
    vec3 v = normalize(-position);
    vec3 s = dirToLight(position);
	return lightIntensity(position, s)
        * ( _diffuse(diffuseColor, s, normal)
            + _specular(specularColor, shininess, v, s, normal)
        );
}

vec3 diffuseSpecular(
    Material material, vec3 normal, vec3 position
){
    return diffuseSpecular(
        material.diffuse, material.specular, material.shininess,
        normal, position
    );
}

vec3 diffuseSpecular(vec3 normal, vec3 position){
    return diffuseSpecular(material, normal, position);
}

vec3 diffuseSpecular(vec2 uv, vec3 normal, vec3 position){
    vec3 diffColor = material.useDiffuseMap
        ? texture2D(diffuseMap, uv).rgb
        : material.diffuse;
    vec3 specColor = material.useSpecularMap
        ? texture2D(specularMap, uv).rgb
        : material.specular;
    return diffuseSpecular(
        diffColor, specColor, material.shininess,
        normal, position
    );
}


/**
 * Only specular contribution of above
 */


vec3 specularEx(
    vec3 specularColor, float shininess, vec3 normal, vec3 v, vec3 s, vec3 flux
){
    return flux * _specular(specularColor, shininess, v, s, normal);
}

vec3 specularEx(vec3 normal, vec3 v, vec3 s, vec3 flux){
    return specularEx(material.specular, material.shininess, normal, v, s, flux);
}

 vec3 specularEx(vec2 uv, vec3 normal, vec3 v, vec3 s, vec3 flux){
    if(!material.useSpecularMap) return specularEx(normal, v, s, flux);
    else {
        vec3 specColor = texture2D(specularMap, uv).rgb;
        return specularEx(specColor, material.shininess, normal, v, s, flux);
    }
 }




vec3 _diffuse(vec3 diffuseColor, vec3 s, vec3 normal){
    return diffuseColor  * lambertian(s, normal);
}

vec3 _specular(vec3 specularColor, float shininess, vec3 v, vec3 s, vec3 normal){
    return specularColor * phongSpec(v,s,normal,shininess);
}
