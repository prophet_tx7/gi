layout (location = 0) in vec3 probePosition;
layout (location = 1) in vec3 probeNormal;

uniform uint firstProbeID;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;  

uniform uint shadingBufferDim;

out vec3 normal;
out vec3 position;

void main(){
    
    gl_Position = probeIdToClipSpace(firstProbeID + uint(gl_VertexID), shadingBufferDim);
    normal      = normalize(normalMatrix*probeNormal);
    position    = vec3(modelViewMatrix*vec4(probePosition, 1.0));

}
