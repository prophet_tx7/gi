in vec3 normal;
in vec3 position;
in vec2 texCoords;

layout (location = 1) out vec3 normalData;
layout (location = 2) out vec3 positionData;
layout (location = 3) out vec3 fluxData;

void main(){
	normalData      = normalize(normal);
	positionData    = position;
    fluxData        = diffuse(texCoords, normalize(normal), position);
}
