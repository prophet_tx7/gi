in vec3 position; // eye space
in vec3 normal;
in vec4 shadowCoord;
in vec2 texCoords;

uniform vec3    ambientLight;


layout (location=0) out vec4 fragColor;

void main(){
    fragColor = vec4(
        diffuseSpecular(texCoords, normalize(normal), position)*shadowCoef(shadowCoord),
        1.0
    );

    vec3 diffColor = material.useDiffuseMap
        ? texture2D(diffuseMap, texCoords).rgb
        : material.diffuse;
    fragColor.rgb +=  diffColor*ambientLight;
}
