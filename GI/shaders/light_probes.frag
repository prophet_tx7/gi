uniform sampler2D rsmPositionTex;
uniform sampler2D rsmNormalTex;
uniform sampler2D rsmFluxTex;

uniform uint    rsmDim;
uniform bool    enableSpecular;
uniform float   shininess; 

in vec3 normal; // normalized
in vec3 position;

layout (location=0) out vec3 outProbePos;
layout (location=1) out vec3 outProbeNormal;
layout (location=2) out vec3 outDiffVplPos;
layout (location=3) out vec3 outDiffVplFlux;
layout (location=4) out vec3 outSpecVplPos;
layout (location=5) out vec3 outSpecVplFlux;

void main(){

    outDiffVplPos   = vec3(0);
    outDiffVplFlux  = vec3(0);
    outSpecVplPos   = vec3(0);
    outSpecVplFlux  = vec3(0);

    const float epsilon = 1e-12;

    float Wdiff = 0;
    float Wspec = 0;

    float rsmStep = 1.f/rsmDim;

    vec3 v = normalize(-position);

    vec2 uv;
    for(uv.x=rsmStep/2; uv.x<=1.f; uv.x+=rsmStep){
        for(uv.y=rsmStep/2; uv.y<=1.f; uv.y+=rsmStep){

            vec3    valPos          = texture(rsmPositionTex, uv).xyz;
            vec3    valNormal       = texture(rsmNormalTex, uv).xyz;
            vec3    valFlux         = texture(rsmFluxTex, uv).rgb;

            float w;
            float wcos = diskToPoint(valPos, valNormal, position, normal, rsmStep, w);


            outDiffVplFlux  += wcos*valFlux;
            outDiffVplPos   += valPos*(epsilon+wcos);
            Wdiff           += epsilon+wcos;
            
            if(enableSpecular){
                vec3  s         = normalize(valPos - position);
                float wspec     = w * phongSpec(v, s, normal, shininess);
                outSpecVplFlux  += wspec*valFlux;
                outSpecVplPos   += valPos*(epsilon+wspec);
                Wspec           += epsilon+wspec;
            }

        }
    }


    outDiffVplPos /= Wdiff;
    // renormalization
    vec3 diff = outDiffVplPos-position;
    float distSq = dot(diff,diff);
    vec3 dir = normalize(diff);
    outDiffVplFlux *= (distSq)/(max(dot(dir,normal),0.00001f));

    if(enableSpecular){
        outSpecVplPos /= Wspec;
        // renormalization
        diff = outSpecVplPos-position;
        distSq = dot(diff,diff);
        dir = normalize(diff);
        outSpecVplFlux *= (distSq)/(max(phongSpec(v, dir, normal, 10),0.00001f));
    }

    outProbePos = position;
    outProbeNormal = normal;

}
