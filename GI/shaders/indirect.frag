uniform uint    numProbes;
uniform bool    enableSpecular;

in vec3 normal;
in vec3 position;
in vec2 texCoords;

in vec3 dirToDiffVpl;
in vec3 dirToSpecVpl;
in vec3 diffVplFlux;
in vec3 specVplFlux;

layout(location=0) out vec4 fragColor;

void main(){

    fragColor = vec4(0,0,0,1);

    vec3 normal1 = normalize(normal);
    fragColor.rgb += 
        diffuseEx(texCoords, normal1, dirToDiffVpl, diffVplFlux);

    if(!enableSpecular) return;

    fragColor.rgb +=
        specularEx(texCoords, normal1, normalize(-position), dirToSpecVpl, specVplFlux);

}
