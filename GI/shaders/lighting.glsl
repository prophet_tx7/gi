/**
 * Common lighing functions, definitions and uniform (without shadows)
 */

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif


/* Light definitions *********************************************************/


#define LIGHT_TYPE_SPOT			0
#define LIGHT_TYPE_POINT		1
#define LIGHT_TYPE_DIRECTIONAL	2

struct Light{
    int	    type;
    vec3    color;
    float   intensity;
    vec3    position;   // in eye-space
	float   cutoff;     // [0..M_PI/2.0]
    vec3    direction;
    float   spotExponent;
    float   spotFade;       // 0..1, where fadout starts,
                            // 1 ~ no fade out (sharp border)
};

uniform Light   light;

/*****************************************************************************/


vec3 dirToLight(vec3 position){
	if( light.type==LIGHT_TYPE_DIRECTIONAL ) {
		return light.direction;
	} else {
		return normalize(vec3(light.position-position));
	}
}


vec3 lightIntensity(vec3 eyeCoords, vec3 s){

	if(light.type==LIGHT_TYPE_SPOT){

		float angle = acos(dot(-s,light.direction));
		float cutoff = clamp(light.cutoff, 0.0, M_PI/2.0);
		if(angle<=cutoff) {
            float spotFactor = pow( dot(-s, light.direction), light.spotExponent );
            spotFactor *= 1.f-smoothstep( cutoff*light.spotFade, cutoff, angle);
            return  spotFactor * light.color*light.intensity;
        }

	}

	return vec3(0);

}


/**
 * Computes disk-to-point factor from given disk (VAL) and point properties.
 * It uses rsmStep and global light properties to compute disk (VAL) radius.
 * It also computes VAL weight for the VAL avereging scheme.
 */
float diskToPoint(
    in vec3 valPos, in vec3 valNormal,
    in vec3 pointPos, in vec3 pointNormal,
    in float rsmStep,
    out float valWeight
){

        vec3    posDiff         = valPos-pointPos;
        vec3    dir             = normalize(posDiff);
        float   cos1            = max(0.f, dot(-dir,valNormal));
        float   distSq          = dot(posDiff,posDiff);
        float   distFromLightSq = dot(light.position-valPos, light.position-valPos);
        float   tangens         = tan(light.cutoff);
        float   diskRadiusSq    = 4*distFromLightSq*tangens*tangens*rsmStep*rsmStep/M_PI;

        valWeight = diskRadiusSq*cos1/(20.f*diskRadiusSq+distSq); // 20 found experimentally

        return valWeight * max( 0.f, dot(dir,pointNormal) ); // = weight * cos2

}

float lambertian(vec3 s, vec3 normal){
    return max(dot(s,normal), 0.0);
}

float phongSpec(vec3 v, vec3 s, vec3 normal, float shininess){
    vec3 h = normalize(v+s);    
    return pow(max(dot(h,normal), 0), shininess/*4.0*/); // 4 - phong 2 blinn
}
