/**
 * Basic vertex transformations.
 * It transforms vertexPosition by given mvp, define additional macros
 * for additional transformations:
 *  - ALL_ATTRIBUTES
 *      vertexNormal and vertexTexCoord is also transformed,
 *      also eye space position is computed
 *  - SHADOW_COORDS
 *      coordinate in shadow map is also computed
 */



/* Common uniforms, input variables and output variables *********************/

uniform mat4 mvp;  // used to compute gl_Position

layout (location=0) in vec3 vertexPosition;
layout (location=1) in vec3 vertexNormal;
layout (location=2) in vec2 vertexTexCoords;

/* Specific uniforms and output variables ************************************/

#ifdef ALL_ATTRIBUTES
uniform mat4 modelViewMatrix; // used to compute out position
uniform mat3 normalMatrix;    // used to compute out normal

out vec3 normal;   // eye space
out vec3 position; // eye space
out vec2 texCoords;
#endif


#ifdef SHADOW_COORDS
uniform mat4 shadowMatrix; // transformation from object space coordinates to shadow map coordinates
out vec4 shadowCoord;
#endif


#ifdef LIGHT_SKIN
const uint numProbesPerVertex = 8u;

uniform sampler2D   probePosTex;
uniform sampler2D   probeNormalTex;
uniform sampler2D   probeDiffVplPosTex;
uniform sampler2D   probeDiffVplFluxTex;
uniform sampler2D   probeSpecVplPosTex;
uniform sampler2D   probeSpecVplFluxTex;
uniform uint        bufferDim;
uniform uint        firstProbeId;

layout (location=3) in uint probeIndices[numProbesPerVertex];


out vec3 diffVplFlux;
out vec3 specVplFlux;
out vec3 dirToDiffVpl;
out vec3 dirToSpecVpl;
#endif

/*****************************************************************************/



void main(){
    

    gl_Position = mvp * vec4(vertexPosition, 1.0);


    #ifdef ALL_ATTRIBUTES
    normal      = normalMatrix * vertexNormal;
    position    = vec3(modelViewMatrix * vec4(vertexPosition, 1.0));
    texCoords   = vertexTexCoords;
    #endif


    #ifdef SHADOW_COORDS
    shadowCoord = shadowMatrix * vec4(vertexPosition, 1.0);
    #endif


    #ifdef LIGHT_SKIN

    vec2    probeUVs[numProbesPerVertex];
    float   probeDistances[numProbesPerVertex];
    float   maxDistance = 0;
    
    vec3 diffVplPosition    = vec3(0);
    vec3 specVplPosition    = vec3(0);
    diffVplFlux             = vec3(0);
    specVplFlux             = vec3(0);

    for(uint i=0u; i<numProbesPerVertex; i++){
        probeUVs[i]         = probeIdToUv(firstProbeId+probeIndices[i], bufferDim);
        vec3 probePosition  = texture2D(probePosTex, probeUVs[i]).xyz;
        vec3 diff           = position - probePosition;
        probeDistances[i]   = sqrt(dot(diff,diff));
        if(probeDistances[i] > maxDistance) maxDistance = probeDistances[i];
    }

    float totalWeight = 0;
    for(uint i=0u; i<numProbesPerVertex; i++){
        
        vec3    probeNormal         = texture2D(probeNormalTex, probeUVs[i]).xyz;
        vec3    probeDiffVplFlux    = texture2D(probeDiffVplFluxTex, probeUVs[i]).rgb;
        vec3    probeDiffVplPos     = texture2D(probeDiffVplPosTex, probeUVs[i]).xyz;
        vec3    probeSpecVplFlux    = texture2D(probeSpecVplFluxTex, probeUVs[i]).rgb;
        vec3    probeSpecVplPos     = texture2D(probeSpecVplPosTex, probeUVs[i]).xyz;
        float   weight              = (1-probeDistances[i]/maxDistance)*max(0f,dot(probeNormal,normal));
        
        totalWeight += weight;
        diffVplPosition += weight*probeDiffVplPos;
        diffVplFlux     += weight*probeDiffVplFlux;
        specVplPosition += weight*probeSpecVplPos;
        specVplFlux     += weight*probeSpecVplFlux;
    
    }
    
    diffVplPosition /= totalWeight;
    diffVplFlux     /= totalWeight;
    specVplPosition /= totalWeight;
    specVplFlux     /= totalWeight;

    // my renormalization to unit distance

    vec3 diff = diffVplPosition - position;
    float dist = sqrt(dot(diff,diff));
    dirToDiffVpl = diff/dist;
    diffVplFlux /= dist*dist;

    diff = specVplPosition - position;
    dist = sqrt(dot(diff,diff));    
    dirToSpecVpl = diff/dist;
    specVplFlux /= dist*dist;
    
    #endif

}
