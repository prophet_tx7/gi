#pragma once

#include <exception>

#ifdef _MSC_VER
#if _MSC_VER < 1900
#define noexcept throw()
#endif
#endif

/**
* All exceptions connected with a renderer are derived
* from this class
*/
class RendererException : public std::exception {

public:

    char* details;

    RendererException(const char* details)
        : exception() {
        construct(details);
    }

	RendererException() : RendererException(NULL) {}

    RendererException(const RendererException& orig) {
        construct(orig.details);
    }

    ~RendererException() {
        delete[] details;
    }

    virtual const char* what() const noexcept override {
        return "RendererException";
    }

private:

    void construct(const char* details) {
        if (!details) {
            this->details = NULL;
            return;
        }
        size_t strlenwz = strlen(details) + 1;
        this->details = new char[strlenwz];
        memcpy(
            this->details, details,
            (strlenwz)* sizeof(char)
            );
    }

};
