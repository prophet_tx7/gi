#include "GLRenderer.h"

GLRenderer::GLRMBufferDebug::GLRMBufferDebug() {

    GLShader vertex(GL_VERTEX_SHADER, "330", "RenderBuffers Debugging VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "RenderBuffers Debugging FragmentShader");

    vertex.addSourceFromFile("shaders/pass-through.vert");
    fragment.addSourceFromFile("shaders/just_texture.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();

}


void GLRenderer::GLRMBufferDebug::use() {
    m_program.use();
}


void GLRenderer::GLRMBufferDebug::setBufferID(uint bufferID) {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, bufferID);
    m_program.setUniform("buffer", 0);
}
