#include "GLRenderer.h"

GLRenderer::GLRMDeferred::GLRMDeferred(const GBuffer& gbuffer)
    : RMDeferred(gbuffer), m_rmiLight(m_program)
{

    GLShader vertex(GL_VERTEX_SHADER, "330", "Deferred VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "Deferred FragmentShader");
    
    vertex.addSourceFromFile("shaders/pass-through.vert");

    //fragment.addSource("#define SHADOW_MAPPING");
    fragment.addSourceFromFile("shaders/color.glsl");
    fragment.addSourceFromFile("shaders/lighting.glsl");
    fragment.addSourceFromFile("shaders/shading.glsl");
    fragment.addSourceFromFile("shaders/shadow_mapping.glsl");
    fragment.addSourceFromFile("shaders/deferred_shading.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();
    
}

void GLRenderer::GLRMDeferred::use() {

    m_program.use();
    
    // G-Buffer textures

    m_program.setUniform("gbNormalTex",   0);
    m_program.setUniform("gbPositionTex", 1);
    m_program.setUniform("gbRgbTex",      2);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_gbuffer.getNormalBufferID());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_gbuffer.getPositionBufferID());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, m_gbuffer.getMaterialBufferID());

}

void GLRenderer::GLRMDeferred::enableDirectLighting(bool enable) {
    m_program.setUniform("direct", enable);
}

void GLRenderer::GLRMDeferred::setRSM(const RSM* rsm) {

    if (rsm) {

        // there is a standard shadow map at texture unit no. 3

        m_program.setUniform("indirectDiffuse", m_indirectDiffuse);
        m_program.setUniform("indirectSpecular", m_indirectSpecular);
        m_program.setUniform("bufferDim", rsm->getRenderBuffers().getWidth());

        m_program.setUniform("rsmNormalTex", 4);
        m_program.setUniform("rsmPositionTex", 5);
        m_program.setUniform("rsmRgbTex", 6);

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, rsm->getNormalBufferID());
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, rsm->getPositionBufferID());
        glActiveTexture(GL_TEXTURE6);
        glBindTexture(GL_TEXTURE_2D, rsm->getFluxBufferID());

    } else {
        m_program.setUniform("indirectDiffuse", false);
        m_program.setUniform("indirectSpecular", false);
    }

}

Renderer::RMILight& GLRenderer::GLRMDeferred::lightInterface() {
    return m_rmiLight;
}

void GLRenderer::GLRMDeferred::enableIndirectLighting(bool diffuse, bool specular) {
    m_indirectDiffuse = diffuse;
    m_indirectSpecular = specular;
    m_program.setUniform("indirectDiffuse", diffuse);
    m_program.setUniform("indirectSpecular", specular);
}
