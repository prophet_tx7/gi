#pragma once
#include "PlaneMesh.h"
#include "gtx/transform.hpp"

using namespace glm;

class BoxMesh : public Mesh{

public:

    BoxMesh(
        float sizeX, float sizeY, float sizeZ,
        uint tesX, uint tesY, uint tesZ
    ){
        m_numVertices
            = 2 * (tesX + 1)*(tesY + 1)
            + 2 * (tesX + 1)*(tesZ + 1)
            + 2 * (tesY + 1)*(tesZ + 1);
        m_numIndices = 6 * 2*(tesX*tesY + tesX*tesZ + tesY*tesZ);
        m_vertices = vertices_t(new VertexPNT[m_numVertices]);
        m_indices = indices_t(new uint[m_numIndices]);

        // generate 6 planes
        mat4 transform;
        uint iVertex = 0;
        uint iIndex = 0;
        //XZ
        transform = translate(vec3(0, sizeY/2.f, 0));
        transform = rotate(transform, -half_pi<float>(), vec3(1.f, 0.f, 0.f));
        createOpositePlanes(
            sizeX, sizeZ, tesX, tesZ,
            transform, vec3(1,0,0),
            iVertex, iIndex
            );
        //XY
        transform = translate(vec3(0, 0, sizeZ/2.f));
        createOpositePlanes(
            sizeX, sizeY, tesX, tesY,
            transform, vec3(1, 0, 0),
            iVertex, iIndex
            );
        //YZ
        transform = translate(vec3(sizeX/2, 0, 0));
        transform = rotate(transform, half_pi<float>(), vec3(0, 1, 0));
        createOpositePlanes(
            sizeZ, sizeY, tesZ, tesY,
            transform, vec3(0, 0, 1),
            iVertex, iIndex
            );


    }

private:

    void createOpositePlanes(
        float sizeX, float sizeY, uint tesX, uint tesY,
        const mat4& transform, const vec3&  rotationAxis,
        uint& iVertex, uint& iIndex
    ) {

        int numVertices = (tesX + 1)*(tesY + 1);

        auto vertices = m_vertices.get() + iVertex;
        auto indices = m_indices.get() + iIndex;

        PlaneMesh::createPlaneMesh(
            sizeX, sizeY, tesX, tesY,
            vertices,
            indices,
            iVertex,
            transform
            );

        PlaneMesh::createPlaneMesh(
            sizeX, sizeY, tesX, tesY,
            vertices + numVertices,
            indices + 6*tesX*tesY,
            iVertex + numVertices,
            rotate(pi<float>(), rotationAxis)*transform
            );

        iVertex += 2 * numVertices;
        iIndex += 12 * tesX*tesY;
    }

};
