#pragma once
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"

using glm::mat4;
using glm::vec3;

class Camera {


public:

	Camera(
		const vec3& eye, const vec3& target, const vec3& up,
		const float aspectRatio,
        const float near, const float far
    );



    const mat4& getViewMatrix() const {
        return m_view;
    };

    const mat4& getViewProjectionMatrix() const {
        return m_viewProj;
    };



    // TODO - review this, consider using Transform class:

    void rotateZ(float angle);

    void rotateY(float angle);

    void translateForward(float distance);

    void translateRight(float distance);




private:

	mat4 m_view;
	mat4 m_proj;
	mat4 m_viewProj;


};
