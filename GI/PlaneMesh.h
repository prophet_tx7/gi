#pragma once

#include "Mesh.h"

using glm::vec3;
using glm::mat4;


class PlaneMesh : public Mesh{

public:

    /**
     * Creates a plane mesh 
     *
     * @param dimX size along X-axis
     * @param dimY size along Y-axis
     * @param tesX number of polygons along X-axis
     * @param tesY number of polygons alogn Y-axis
     */
    PlaneMesh(float dimX, float dimY, uint tesX, uint tesY);

    static void createPlaneMesh(
        float dimX, float dimZ,
        uint tesX, uint tesZ,
        VertexPNT* outVertices,
        uint* outIndices,
        uint iFirstVertex,
        const mat4& transform
    );

    static void createPlaneProbes(
        float dimX, float dimY,
        uint numProbesX, uint numProbesY,
        VertexPN* outProbes,
        const mat4& transform
    );

protected:


    PlaneMesh(){}

private:

    /**
     *  Puts 6 indices of quad at a specified position in the plane
     * to a specified place in the memory
     *
     * @param x quad position in the plane
     * @param y quad position in the plane
     * @param tesX number of quads in x-direction
     * @param tesY number of quads in z-direction
     * @param indices an address of the first index of the plane
     */
    static void indexQuad(
        uint x, uint y,
        uint tesX, uint tesY,
        uint* indices,
        uint iFirstVertex
    ){
        // corner indices in the vertex buffer
        uint iTL = iFirstVertex + x*(tesY + 1) + y;
        uint iTR = iFirstVertex + (x + 1)*(tesY + 1) + y;
        uint iBL = iTL + 1;
        uint iBR = iTR + 1;
        // the index of the quad's first index
        uint ii = (x*tesY+y)*6;
        // first triangle
        indices[ii + 0] = iTL;
        indices[ii + 1] = iBL;
        indices[ii + 2] = iBR;
        // second triangle
        indices[ii + 3] = iBR;
        indices[ii + 4] = iTR;
        indices[ii + 5] = iTL;
    }

};
