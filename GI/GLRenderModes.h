#pragma once
#include <detail/_vectorize.hpp>
#include <detail/_vectorize.hpp>

class GLRenderer::GLRMBufferDebug : public RMBufferDebug {

public:

    GLRMBufferDebug();

    void use() override;
    void setBufferID(uint bufferID) override;
private:

    GLProgram m_program;
};

/**
 * Forward rendering mode
 */
class GLRenderer::GLRMForward : public RMForward{

public:

    GLRMForward();

    void use() override;

    RMITransform& transformInterface() override;
    RMILight& lightInterface() override;

    void enableAmbient(bool enable) override;

private:

    GLProgram       m_program;
    GLRMITransform  m_rmiTransform;
    GLRMILight      m_rmiLight;
	GLRMIMaterial   m_rmiMaterial;


	// Inherited via RMForward
	virtual RMIMaterial& materialInterface() override;

};



/**
 * Shadow map creation mode
 */
class GLRenderer::GLRMShadowMap : public RMShadowMap {

public:

    GLRMShadowMap();

    void use() override;
    void setMVP(const mat4& mvp) override;

private:

    GLProgram m_program;

};


/**
 * Reflective shadow map creation mode
 */
class GLRenderer::GLRMRSM : public RMRSM {

public:

    GLRMRSM();

    void use() override;
    void setCameraViewMatrix(const mat4& viewMatrix) override;
    void setLight(const Light& light) override;
    void setModelMatrix(const mat4& mvp) override;

private:

    GLProgram       m_program;
    const Light*    m_light;
    mat4            m_lightViewProjMatrix;
    mat4            m_cameraViewMatrix;
	GLRMIMaterial	m_rmiMaterial;


	// Inherited via RMRSM
	virtual RMIMaterial & materialInterface() override;

};



/**
 * G-buffer creation mode
 */
class GLRenderer::GLRMGBuffer : public RMGBuffer {

public:

    GLRMGBuffer();

    void use() override;
    RMITransform& transformInterface() override;

private:

    GLProgram       m_program;
    GLRMITransform  m_rmiTransform;
	GLRMIMaterial	m_rmiMaterial;


	// Inherited via RMGBuffer
	virtual RMIMaterial& materialInterface() override;

};

/**
 * Deferred shading render mode (using given G-Buffer)
 */
class GLRenderer::GLRMDeferred : public RMDeferred {

public:
    void enableIndirectLighting(bool diffuse, bool specular) override;

    explicit GLRMDeferred(const GBuffer& gbuffer);

    void use() override;

    void enableDirectLighting(bool enable) override;
    RMILight& lightInterface() override;
    void setRSM(const RSM* rsm) override;

private:

    GLProgram   m_program;
    GLRMILight  m_rmiLight;
    bool        m_indirectDiffuse;
    bool        m_indirectSpecular;

};


class GLRenderer::GLRMProbes : public RMProbes {

public:

    GLRMProbes(const RSM& rsm);
    void use() override;
    void enableSpecular(bool enable) override;
    virtual void setShadingBufferDim(uint dim) override;
    virtual void setCameraViewMatrix(const mat4& viewMatrix) override;
    virtual void setLight(const Light& light) override;
    void setFirstProbeID(uint firstProbeID) override;
    virtual void setMatrices(const mat4& modelViewMatrix, const mat3& normalMatrix) override;
    void setMaterialShininess(const float shininess) override;

private:

    GLProgram   m_program;
    mat4        m_cameraViewMatrix;
    GLRMILight  m_rmiLight;
};


class GLRenderer::GLRMIndirect : public RMIndirect {

public:

    explicit GLRMIndirect(const ShadingBuffers& shadingBuffers);

    void use() override;
    void enableSpecular(bool enable) override;
    RMITransform& transformInterface() override;
    RMIMaterial& materialInterface() override;
    void setFirstProbeID(uint firstProbeId) override;

private:

    GLProgram       m_program;
    GLRMITransform  m_rmiTransform;
    GLRMIMaterial   m_rmiMaterial;

};
