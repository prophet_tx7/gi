#include "GLRenderer.h"

GLRenderer::GLRenderBuffers::GLRenderBuffers(uint width, uint height)
    : RenderBuffers(width, height)
{
    
    m_iFBO = 0;
    m_iDepthTexture = 0;
    m_iLastTexture = GL_TEXTURE0;
    m_iLastColorAttachment = GL_COLOR_ATTACHMENT0;

    glGenFramebuffers(1, &m_iFBO);

}


GLRenderer::GLRenderBuffers::~GLRenderBuffers() {
    if (m_iFBO)             glDeleteFramebuffers(1, &m_iFBO);
    if (m_iDepthTexture)    glDeleteTextures(1, &m_iDepthTexture);
}


void GLRenderer::GLRenderBuffers::createDepthBuffer() {

    const GLfloat border[] = { 1.0f, 0.0f, 0.0f, 0.0f };

    createTexture(
        GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE,
        m_iDepthTexture
    );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
        GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    setupFBO(GL_DEPTH_ATTACHMENT, m_iDepthTexture, GL_NONE);

}



uint GLRenderer::GLRenderBuffers::createBuffer(BufferFormat textureFormat) {

    GLenum internalFormat, format, type;

    switch(textureFormat){
    case RGB8:
        internalFormat = GL_RGB8;
        format = GL_RGB;
        type = GL_BYTE;
        break;
    case RGB32F:
        internalFormat = GL_RGB32F;
        format = GL_RGB;
        type = GL_FLOAT;
        break;
    case RGBA8:
        internalFormat = GL_RGBA8;
        format = GL_RGBA;
        type = GL_BYTE;
        break;
    case RGBA32F:
        internalFormat = GL_RGBA32F;
        format = GL_RGBA;
        type = GL_FLOAT;
        break;
    }

    uint ret;
    createTexture(internalFormat, format, type, ret);

    setupFBO(m_iLastColorAttachment++, ret);

    return ret;

}



void GLRenderer::GLRenderBuffers::bind() {

    glBindFramebuffer(GL_FRAMEBUFFER, m_iFBO);
    glViewport(0, 0, m_width, m_height);

}



void GLRenderer::GLRenderBuffers::createTexture(
    GLenum internalFormat, GLenum format, GLenum type,
    GLuint &iTexture
) {

    glActiveTexture(m_iLastTexture++);
    glGenTextures(1, &iTexture);
    glBindTexture(GL_TEXTURE_2D, iTexture);
    glTexImage2D(
        GL_TEXTURE_2D, 0, internalFormat, m_width, m_height,
        0, format, type, NULL
    );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

}



void GLRenderer::GLRenderBuffers::setupFBO(GLenum attachment, GLuint iTexture, GLenum drawBufferMode) {
    glBindFramebuffer(GL_FRAMEBUFFER, m_iFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, iTexture, 0);
	m_drawBuffers.push_back(drawBufferMode);
	glDrawBuffers(m_drawBuffers.size(), m_drawBuffers.data());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void GLRenderer::GLRenderBuffers::setupFBO(GLenum attachment, GLuint iTexture){
	setupFBO(attachment, iTexture, attachment);
}
