#include "GLRenderer.h"


GLRenderer::GLObject3D::GLObject3D(const Object3D& object) : RObject3D(object) {

    initMeshBuffers(object.getModel());
    initProbeBuffers(object.getModel());

}

GLRenderer::GLObject3D::~GLObject3D() {
    glDeleteVertexArrays(1, &m_iMeshVAO);
    glDeleteBuffers(1, &m_iMeshVBO);
    glDeleteBuffers(1, &m_iMeshVBOelements);
}


void GLRenderer::GLObject3D::drawMesh() {
    glBindVertexArray(m_iMeshVAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iMeshVBOelements);
    glDrawElements(
        GL_TRIANGLES,
        m_object.getModel().getMesh().getNumIndices(),
        GL_UNSIGNED_INT,
        NULL
    );
}


void GLRenderer::GLObject3D::drawLightProbes() {
    glBindVertexArray(m_iProbesVAO);
    glPointSize(1);
    glDrawArrays(GL_POINTS, 0, m_object.getModel().getNumProbes());
}


void GLRenderer::GLObject3D::initMeshBuffers(const Model3D& model) {

    auto& mesh = model.getMesh();

    m_iMeshVBO = createAndBindVBO(mesh.getSizeOfVertices(), mesh.getVertices());

    // VAO
    glGenVertexArrays(1, &m_iMeshVAO);
    glBindVertexArray(m_iMeshVAO);
    glEnableVertexAttribArray(0); // Vertex position
    glEnableVertexAttribArray(1); // Normal
    glEnableVertexAttribArray(2); // Texture coordinates
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNT), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNT), (GLvoid*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexPNT), (GLvoid*)(6 * sizeof(float)));
    
    // Probe indices
    if(model.getLightProbes()) {
        const auto nProbesPerVert = model.getNumProbesPerVertex();
        m_iProbeIndicesVBO = createAndBindVBO(
            mesh.getNumVertices()*nProbesPerVert*sizeof(uint),
            model.getLightProbesIndices()
        );
        for (uint i=0; i<nProbesPerVert; i++) {
            glEnableVertexAttribArray(3+i);
            glVertexAttribIPointer(
                3+i, 1, GL_UNSIGNED_INT,
                nProbesPerVert*sizeof(uint),
                (void*)(i*sizeof(uint))
            );
        }
    }

    // Indices - elements
    glGenBuffers(1, &m_iMeshVBOelements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iMeshVBOelements);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        mesh.getSizeOfIndices(),
        mesh.getIndices(),
        GL_STATIC_DRAW
    );
    if (glGetError() == GL_OUT_OF_MEMORY) {
        glDeleteVertexArrays(1, &m_iMeshVAO);
        glDeleteBuffers(1, &m_iMeshVBO);
        throw OutOfMemoryException();
    }
}


void GLRenderer::GLObject3D::initProbeBuffers(const Model3D& model) {
    
    const auto lightProbes = model.getLightProbes();
    if (!lightProbes) return; // no light probes set

    createAndBindVBO(model.getSizeOfLightProbes(), model.getLightProbes());

    // VAO
    glGenVertexArrays(1, &m_iProbesVAO);
    glBindVertexArray(m_iProbesVAO);
    glEnableVertexAttribArray(0); // Probe position
    glEnableVertexAttribArray(1); // Normal
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), (GLvoid*)(3*sizeof(float)));

}


GLuint GLRenderer::GLObject3D::createAndBindVBO(GLsizeiptr size, const GLvoid* data) {
    GLuint iVBO;
    glGenBuffers(1, &iVBO);
    glBindBuffer(GL_ARRAY_BUFFER, iVBO);
    glBufferData( GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    if (glGetError() == GL_OUT_OF_MEMORY) throw OutOfMemoryException();
    return iVBO;
}
