#include "GLShader.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;


GLShader::GLShader(const GLenum shaderType, const char* version, std::string name = "") {

    m_handle = 0;
    m_shaderType = shaderType;
    m_numSourceFiles = 0;
    
    string strVersionLine("#version ");
    strVersionLine += version;
    strVersionLine += "\n";
    GLchar* versionLine = new GLchar[strVersionLine.size()+1];
    memcpy(versionLine, strVersionLine.c_str(), strVersionLine.size());
    versionLine[strVersionLine.size()] = 0;
    m_sources.push_back(versionLine);

    m_name = name;

}


void GLShader::addSource(const char* source) {

    stringstream buffer;
    buffer << "#line 1 " << (m_sources.size() - 1) << '\n';
    buffer << source << '\n';

    GLchar* code = new GLchar[buffer.str().length() + 1];
    memcpy(code, buffer.str().c_str(), buffer.str().length() + 1);
    m_sources.push_back(code);

}


void GLShader::addSourceFromFile(const char* fileName){

    stringstream buffer;
    ifstream f(fileName);
    if (!f.is_open()) throw LoadException(fileName);
    buffer << f.rdbuf();
    if (!f) throw LoadException(fileName);
    f.close();

    addSource(buffer.str().c_str());

}


void GLShader::createAndCompile(){

    if(m_handle) return;

    // create
    m_handle = glCreateShader(m_shaderType);
    if(m_handle == 0) throw GLShader::CreateException();

    // compile
    glShaderSource(m_handle, m_sources.size(), m_sources.data(), NULL);
    glCompileShader(m_handle);
    verifyCompilationStatus();

    releaseSources();

}


void GLShader::release(){
    if(!m_handle) return;
    glDeleteShader(m_handle);
    m_handle = 0;
}


void GLShader::verifyCompilationStatus(){
    
    // check the result
    GLint result;
    glGetShaderiv(m_handle, GL_COMPILE_STATUS, &result);
    if(result == GL_TRUE) return;

    // failed
    string details(m_name);
    GLint logLen;
    glGetShaderiv(m_handle, GL_INFO_LOG_LENGTH, &logLen);
    char * log;
    if(logLen>0){
        if (m_name.length()) details += " compilation log:\n";
        else details = "";
        log = new char[logLen];
        GLsizei written;
        glGetShaderInfoLog(m_handle, logLen, &written, log);
        details += log;
        details += "\n";
        delete[] log;
    }
    throw CompileException(details.c_str());

}


void GLShader::releaseSources() {
    for (size_t i = 0; i < m_sources.size(); i++) {
        delete[] m_sources[i];
    }
    m_sources.clear();
}
