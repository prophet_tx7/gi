#pragma once

#include "Scene.h"
#include "WavefrontObjects.h"

template <bool daylight>
class SponzaScene : public Scene {


public:


    explicit SponzaScene(float aspectRatio, float probeSubsetSize = 1.f)
        : m_camera(
            vec3(-1170.f, 130.f, 0.f),
            vec3(88.f, 130.f, -0.f),
            vec3(0.f, 1.f, 0.f),
            aspectRatio,
            1, 10000
        ),
        m_objects("sponza/", "sponza.obj", "sponza_probes.obj", probeSubsetSize)
    {
        if (daylight) {
            m_light.setFade(0.9f);
            m_light.setExponent(0);
        } else {
            m_light.setFade(1.0f);
            m_light.setExponent(200.f);
        }
        m_lights.push_back(&m_light);
    }

    const std::vector<const Object3D*> & getObjects() const override {
        return m_objects.getObjects();
    };

    const std::vector<const Light*> & getLights() const override {
        return m_lights;
    };

    Camera& getCamera() override {
        return m_camera;
    }

    const Camera& getCamera() const override {
        return m_camera;
    };

    void update(float deltaTime) override {
        m_time += deltaTime;
        if(!daylight) m_light.setDir(vec3(2.5f, -1.f, sin(m_time/4.f)));
    }

private:

    WavefrontObjects m_objects;
    Camera m_camera;
    SpotLight m_light {
        daylight ? 3.5f                         : 8.0f,
        daylight ? vec3(0.8f, 0.8f, 1.0f)       : vec3(1.f, 0.95f, 0.9f),
        daylight ? vec3(-600.f, 2000.f, -300.f) : vec3(-1300.f, 300.f, 0.f),
        daylight ? vec3(0.2f,-1.f, 0.f)         : vec3(2.5f,-1.f, 0.f),
        daylight ? 0.82f                        : 0.4f,
        1, 10000
    };

    float m_time = 0;

    std::vector<const Light*> m_lights;


};
