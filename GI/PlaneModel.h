#pragma once

#include "Model3D.h"
#include "PlaneMesh.h"

class PlaneModel : public Model3D {

public:

    PlaneModel(
        float sizeX, float sizeY,
        uint tesX, uint tesY,
        uint numProbesX, uint numProbesY,
        const Material& material,
        float probeSubsetSize = 1.0
    ) : Model3D(m_mesh, material), m_mesh(sizeX, sizeY, tesX, tesY) {
        createProbes(sizeX, sizeY, numProbesX, numProbesY, probeSubsetSize);
    }
    
    PlaneModel(
        float sizeX, float sizeY,
        uint tesX, uint tesY,
        uint numProbesX, uint numProbesY,
        float probeSubsetSize = 1.0
    ) : Model3D(m_mesh), m_mesh(sizeX, sizeY, tesX, tesY) {
        createProbes(sizeX, sizeY, numProbesX, numProbesY, probeSubsetSize);
    };

    PlaneModel(
        float size, uint tes, uint numProbes, const Material& material,
        float probeSubsetSize = 1.0
    ) : PlaneModel(size, size, tes, tes, numProbes, numProbes, material, probeSubsetSize) {}

    PlaneModel(
        float size, uint tes, uint numProbes, float probeSubsetSize = 1.0
    ) : PlaneModel(size, size, tes, tes, numProbes, numProbes, probeSubsetSize) {}

    ~PlaneModel() {
        delete m_probes;
    }

private:

    PlaneMesh   m_mesh;
    ProbeSet*   m_probes;

    void createProbes(float sizeX, float sizeY, uint numProbesX, uint numProbesY, float probeSubsetSize=1.0) {

        auto numProbes = numProbesX*numProbesY;

        m_probes = new ProbeSet(numProbes);

        PlaneMesh::createPlaneProbes(
            sizeX, sizeY, numProbesX, numProbesY, m_probes->probes.get(), mat4()
        );

        m_probes->reduce(probeSubsetSize);

        setLightProbes(*m_probes);

    }

};