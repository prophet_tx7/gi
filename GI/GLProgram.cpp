#include "GLProgram.h"

#include <string>
#include <sstream>
#include "gtc/type_ptr.hpp"

using namespace std;

using glm::vec4;



GLProgram::GLProgram() {
    m_handle = glCreateProgram();
    if (!m_handle) throw CreateException();
}



void GLProgram::attachShader(GLShader& shader){
    shader.createAndCompile();
    glAttachShader(m_handle, shader.getHandle());
}


void GLProgram::link() {

    glLinkProgram(m_handle);

    // check the status
    GLint status;
    glGetProgramiv(m_handle, GL_LINK_STATUS, &status);
    if (status == GL_TRUE) return;

    // failed - get the program log
    GLint logLen;
    glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &logLen);
    if (logLen <= 0) throw LinkException();
    else {
        string details("Link log:\n");
        char* log = new char[logLen];
        GLsizei written;
        glGetProgramInfoLog(m_handle, logLen, &written, log);
        details += log;
        details += "\n";
        delete[] log;
        throw LinkException(details.c_str());
    }

}


void GLProgram::use(){
    glUseProgram(m_handle);
}


void GLProgram::release(){
    if(!m_handle) return;
    glDeleteProgram(m_handle);
    m_handle = 0;
}




GLint GLProgram::getUniformLocation(const char* name) {
    
    auto it = m_uniformLocations.find(name);
    GLint location;
    if (it != m_uniformLocations.end()) location = it->second;
    else {
        location = glGetUniformLocation(m_handle, name);
        if (location<0) throw NonexistentUniformException(name);
        m_uniformLocations[name] = location;
    }

    return location;

}


void GLProgram::setUniform(const char* name, const GLfloat value) {
    const GLint location = getUniformLocation(name);
    glUniform1f(location, value);
}


void GLProgram::setUniform(const char* name, const GLuint value) {
    const GLint location = getUniformLocation(name);
    glUniform1ui(location, value);
}


void GLProgram::setUniform(const char* name, const GLint value) {
    const GLint location = getUniformLocation(name);
    glUniform1i(location, value);
}


void GLProgram::setUniform(const char* name, const vec3& vector) {
    const GLint location = getUniformLocation(name);
    glUniform3fv(location, 1, glm::value_ptr(vector));
}


void GLProgram::setUniform(const char* name, const mat3& matrix) {
    const GLint location = getUniformLocation(name);
    glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}


void GLProgram::setUniform(const char* name, const mat4& matrix) {
    const GLint location = getUniformLocation(name);
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}


template<typename T>
void GLProgram::setUniform(
    const char* arrayName, uint i, const char* memberName,
    const T& value
){
    stringstream ss;
    ss << arrayName << "[" << i << "]" << "." << memberName;
    setUniform(ss.str().c_str(), value);
}
