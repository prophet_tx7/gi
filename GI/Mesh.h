#pragma once

#include "glm.hpp"
#include <memory>

using glm::uint;
using glm::vec2;
using glm::vec3;

/**
 * Vertex data - position and normal
 */
struct VertexPN {
    vec3 position;
    vec3 normal;
};

/**
 * Vertex data - position, normal and texture coordinates
 */
struct VertexPNT : VertexPN {
    vec2 texCoords;

};


/**
 * Mesh data
 */
class Mesh {

public:

    typedef std::unique_ptr<VertexPNT[]>    vertices_t;
    typedef std::unique_ptr<uint[]>         indices_t;

    Mesh() {}

    Mesh(vertices_t vertices, uint numVertices, indices_t indices, uint numIndices)
        : m_vertices(std::move(vertices)), m_numVertices(numVertices),
        m_indices(std::move(indices)), m_numIndices(numIndices)
    {}

    virtual ~Mesh() {}


    const VertexPNT*    getVertices() const     { return m_vertices.get(); } 
    uint                getNumVertices() const  { return m_numVertices; }
    const uint*         getIndices() const      { return m_indices.get(); }
    uint                getNumIndices() const   { return m_numIndices; }

    /**
     * Gets size of vetices in bytes
     */
    virtual size_t getSizeOfVertices() const    {return m_numVertices*sizeof(VertexPNT);}

    /**
     * Gets size of indices in bytes
     */
    size_t getSizeOfIndices() const     {return m_numIndices*sizeof(uint);}

protected:

    vertices_t  m_vertices = nullptr;
    indices_t   m_indices = nullptr;
    uint        m_numVertices = 0;
    uint        m_numIndices = 0;

};
