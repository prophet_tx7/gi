/**
 * RObject3D implementation for GLRenderer
 */
class GLRenderer::GLObject3D : public virtual RObject3D {

public:
    /**
     * Immediately creates appropriate buffers on GPU
     * @throws GLRenderer::GLObject3D::OutOfMemoryException
     */
    explicit GLObject3D(const Object3D& object);

    /**
     * Releases the buffers created in the constructor
     */
    ~GLObject3D();

    /**
     * Binds the mesh buffers and calls glDrawElements()
     */
    void drawMesh() override;

    /**
     * Binds the light probes buffers and calls glDrawArrays() with GL_POINTS
     */
    void drawLightProbes() override;

    struct OutOfMemoryException : public RendererException {
        const char* what() const noexcept override {
            return "glBufferData() failed with GL_OUT_OF_MEMORY";
        }
    };

private:

    GLuint m_iMeshVAO = 0;
    GLuint m_iMeshVBO = 0;
    GLuint m_iProbeIndicesVBO = 0;
    GLuint m_iMeshVBOelements = 0;

    GLuint m_iProbesVAO = 0;
    GLuint m_iProbesVBO = 0;

    void initMeshBuffers(const Model3D& model);
    void initProbeBuffers(const Model3D& model);

    GLuint createAndBindVBO(GLsizeiptr size, const GLvoid* data);

};


/**
 * RLight implementation for GLRenderer
 * TODO: is this class useless?
 */
class GLRenderer::GLLight : public virtual RLight {

public:

    GLLight(const Light& light, ShadowMap* sm, RSM* rsm) : RLight(light, sm, rsm) {}

};
