/**
* Contains OpenGL implementation of RMI* (render mode interfaces)
*/

#pragma once
#include "GLProgram.h"

class GLRenderer::GLRMITransform : public RMITransform {

public:
    
    explicit GLRMITransform(GLProgram& program) : m_program(program) {}

    void setMatrices(
        const mat4& modelViewMatrix, const mat3& normalMatrix, const mat4& mvp
    ) override;

private:

    GLProgram& m_program;

};




class GLRenderer::GLRMILight : public RMILight {

public:

    explicit GLRMILight(GLProgram& program) : m_program(program) {}


    void setLight(const mat4& viewMatrix, const Light& light) override;


    void setShadowMatrix(const mat4& shadowMatrix) override;


    void setShadowMap(const ShadowMap& shadowMap) override;

private:

    GLProgram& m_program;

};



class GLRenderer::GLRMIMaterial : public RMIMaterial {

public:

	explicit GLRMIMaterial(GLProgram& program) : m_program(program) {}


	void setMaterial(
        const Material& material,
        const Texture* diffuse = nullptr,
        const Texture* specular = nullptr,
        const bool     setSpecular = true
    ) override;

private:

	GLProgram& m_program;

};
