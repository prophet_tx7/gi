#include <cfloat>

#include "Model3D.h"
#include "PrioritySet.h"
#include <numeric>

const Material Model3D::defaultMaterial;

void Model3D::setLightProbes(
    const ProbeSet& probeSet
) {

    m_probes = &probeSet;
    m_probeIndices = new uint[m_numProbesPerVertex*m_mesh.getNumVertices()];

    PrioritySet<float, uint> set(m_numProbesPerVertex);

    const auto& verts = m_mesh.getVertices();

    for (uint iVert = 0; iVert < m_mesh.getNumVertices(); iVert++) {
        
        set.clear();
        
        for (uint iProbe = 0; iProbe < m_probes->numProbes; iProbe++) {

            const auto& probe   = m_probes->probes[iProbe];
            const auto& vert    = verts[iVert];
            const auto cos      = std::max(0.f, glm::dot(probe.normal, vert.normal));
            const auto diff     = probe.position - vert.position;;
            const auto distSq   = glm::dot(diff, diff);
            const float epsilon = FLT_MIN;
            const auto weight   = cos/(distSq+epsilon);

            set.tryAdd(weight, iProbe);
        }
        memcpy(
            m_probeIndices + iVert*m_numProbesPerVertex,
            set.getDataArr().get(),
            m_numProbesPerVertex * sizeof(uint)
        );

    }

}


void Model3D::ProbeSet::reduce(float probeSubsetSize) {
    
    if (probeSubsetSize >= 1.0) return;

    uint newNumProbes = uint(numProbes * probeSubsetSize);

    std::vector<int> indices(numProbes);
    //srand(SDL_GetTicks());
    std::iota(indices.begin(), indices.end(), 0);
    std::random_shuffle(indices.begin(), indices.end());

    auto newProbes = std::make_unique<VertexPN[]>(newNumProbes);
    for (uint i=0; i<newNumProbes; i++) {
        newProbes[i] = probes[indices[i]];
    }

    probes = std::move(newProbes);
    numProbes = newNumProbes;
    
}
