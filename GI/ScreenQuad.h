#include "PlaneMesh.h"

class Renderer::ScreenQuad : public Object3D {

public:

    explicit ScreenQuad()
        : Object3D(m_model), m_mesh(2.f, 2.f, 1, 1), m_model(m_mesh)
    {}

private:

    PlaneMesh   m_mesh;
    Model3D     m_model;

};
