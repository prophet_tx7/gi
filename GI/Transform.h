#pragma once

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"

using glm::vec3;
using glm::mat4;

class Transform {

public:

	inline void resetIdentity();
	inline void translate(float x, float y, float z);

    /**
     * Rotate around X axis.
     */
	inline void rotateX(float angle);

    /**
     * Rotate around Y axis.
     */
    inline void rotateY(float angle);

    /**
     * Rotate around Z axis.
     */
    inline void rotateZ(float angle);

	inline const mat4& getMatrix() const { return m_matrix; }

private:
	mat4 m_matrix;

};


void Transform::resetIdentity() {
	m_matrix = mat4();
}

void Transform::translate(float x, float y, float z) {
	m_matrix = glm::translate(m_matrix, vec3(x, y, z));
}

void Transform::rotateX(float angle) {
    m_matrix = glm::rotate(m_matrix, angle, vec3(1, 0, 0));
}

void Transform::rotateY(float angle) {
    m_matrix = glm::rotate(m_matrix, angle, vec3(0, 1, 0));
}

void Transform::rotateZ(float angle) {
    m_matrix = glm::rotate(m_matrix, angle, vec3(0, 0, 1));
}
