#pragma once
#include "glm.hpp"
#include <string>

using glm::vec3;
using glm::uint;

struct Material {

    Material() : id(++lastId) {}
    Material(vec3 diffuseColor) : diffuseColor(diffuseColor), id(++lastId) {}
    Material(std::string diffTexFileName, std::string specTexFileName="")
        : diffTexFileName(diffTexFileName),
        specTexFileName(specTexFileName),
        id(++lastId)
    {}

    vec3        diffuseColor    { vec3(1.f) };
    vec3        specularColor   { vec3(0.f) };
    float       shininess       = 10.f;

    std::string diffTexFileName;
    std::string specTexFileName;

    glm::uint getId() const {
        return id;
    }

private:

    static uint     lastId;

    glm::uint       id;

};
