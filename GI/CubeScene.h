#pragma once

#include "Material.h"
#include "BoxModel.h"
#include "PlaneModel.h"
#include "Object3D.h"
#include <vector>
#include "Scene.h"

class CubeScene : public Scene{

public:

    CubeScene(float cameraAspectRatio, float probeSubsetSize = 1.f) :
        camera(
            vec3(0.f, 1.5f, 3.f)*1.1f,
            vec3(0.f),
            vec3(0.f, 1.f, 0.f),
            cameraAspectRatio,
            0.1f, 100.f
        ),
        violetCubeModel{0.75f, 20, 12, violet, probeSubsetSize},
        redCubeModel{1, 20, 12, red, probeSubsetSize},
        yellowCubeModel{1, 20, 12, yellow, probeSubsetSize},
        smallCubeModel{0.5f, 20, 12, green, probeSubsetSize},
        planeModel{ 5, 30, 20 , probeSubsetSize},
        yellowWallModel{ 5, 30, 20, yellow , probeSubsetSize},
        redWallModel{ 5, 30, 20, red , probeSubsetSize},
        blueWallModel{ 5, 30, 20, blue , probeSubsetSize},
        violetCube{ violetCubeModel },
        yellowCube{ yellowCubeModel },
        redCube{ redCubeModel },
        smallCube{ smallCubeModel },
        planeObject{ planeModel },
        leftWall{ redWallModel },
        rightWall{ yellowWallModel },
        backWall{ blueWallModel }
    {
        violetCube.getTransform().translate(0, 0.375f, 0.0f);

        yellowCube.getTransform().translate(-0.75f, 0.5f, -1.5f);
        yellowCube.getTransform().rotateY(-45.f);

        redCube.getTransform().translate(1.5f, 0.5f, -0.75f);
        redCube.getTransform().rotateY(-45.f);

        smallCube.getTransform().translate(-2.f, 0.25f, -1.0f);

        planeObject.getTransform().rotateX(-glm::half_pi<float>());

        leftWall.getTransform().translate(-2.5, 0, 0);
        leftWall.getTransform().rotateY(glm::half_pi<float>());
        rightWall.getTransform().translate(2.5, 0, 0);
        rightWall.getTransform().rotateY(-glm::half_pi<float>());

        backWall.getTransform().translate(0, 0, -2.5f);

        objects.push_back(&violetCube);
        objects.push_back(&yellowCube);
        objects.push_back(&redCube);
        objects.push_back(&smallCube);
        objects.push_back(&planeObject);
        objects.push_back(&leftWall);
        objects.push_back(&rightWall);
        objects.push_back(&backWall);

        light1.setExponent(100);
        lights.push_back(&light1);
        //lights.push_back(&light2);
    }

    const std::vector<const Object3D*>& getObjects() const override {
        return objects;
    }

    const std::vector<const Light*>&    getLights() const override {
        return lights;
    }

    Camera& getCamera() override {
        return camera;
    };

    const Camera& getCamera() const override {
        return camera;
    }

    void update(float deltaTime) override {
        violetCube.getTransform().rotateY(deltaTime / 2.f);
    }

private:


    std::vector<const Object3D*> objects;
    std::vector<const Light*> lights;
    Camera camera;

    Material    blue    {vec3(0.04f, 0.16f, 1.0f)};
    Material    yellow  {vec3(1.0f, 0.64f, 0.09f)};
    Material    red     {vec3(1.0f, 0.09f, 0.09f)};
    Material    green   {vec3(0.04f, 1.0f, 0.16f)};
    Material    violet  {vec3(0.25f, 0.04f, 1.f)};

    BoxModel    violetCubeModel;
    BoxModel    redCubeModel;
    BoxModel    yellowCubeModel;
                               
    BoxModel    smallCubeModel;

    PlaneModel  planeModel;
    PlaneModel  yellowWallModel;
    PlaneModel  redWallModel;
    PlaneModel  blueWallModel;
                               
    Object3D    violetCube;
    Object3D    yellowCube;
    Object3D    redCube;
    Object3D    smallCube;
    Object3D    planeObject;
    Object3D    leftWall;
    Object3D    rightWall;
    Object3D    backWall;
    
    SpotLight light1{
        1.2f,
        vec3(1.f, 0.94f, 0.88f), // warm
        2.f*vec3(1.f, 3.5f, 2.f),
        2.f*vec3(-1.f, -3.5f, -2.f),
        0.44f, 0.1f, 100.f
    };

    SpotLight light2{
        0.5f,
        vec3(0.85f, 0.93f, 1.f), // cold
        vec3(-6.f, 2.f, 1.f),
        vec3(6.f, -2.f, -1.f),
        0.4f, 0.1f, 100.f
    };

};
