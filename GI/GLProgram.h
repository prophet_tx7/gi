#pragma once

#include <map>
#include "glm.hpp"

#include "GLShader.h"
#include "RendererExceptions.h"


using glm::mat3;
using glm::mat4;


/**
 * OpenGL shader program
 */
class GLProgram {


public:

    /**
     * Automatically calls glCreateProgram()
     * @throws GLProgram::CreateException
     */
    GLProgram();

    /**
     * Automatically calls release()
     */
    ~GLProgram() { release();  }



    #pragma region Shader Program Management

    /**
     * Attaches a shader to the program.
     * shader.createAndCompile() will be called automatically.
     *
     * @throws GLShader::CreateException if the shader could not be created
     * @throws GLShader::LoadException if the shader could not be loaded
     * @throws GLShader::CompileException if the shader could not be compiled
     */
    void attachShader(GLShader& shader); 

    /**
     * Links the program.
     * @throws GLProgram::LinkException
     */
    void link();

    /**
     * Calls glDeleteProgram() if a program was created.
     */
    void release();

    /**
     * Simply calls glUseProgram().
     */
    void use();   

    #pragma endregion
    


    #pragma region Uniform management

    /**
     * Acquires an uniform location in the program.
     * Does not check if the program is created!
     *
     * It first searches in AbstractInterface's map for a stored locations
     * if not find it queries OpenGL and stores the location in the map.
     *
     * @throws NonexistentUniformException
     * @return the location in OpenGL
     */
    GLint getUniformLocation(const char* name);

    // ALL FOLLOWING METHODS:
    // - Set an uniform variable in the program
    // - Do not check if the program is created
    // - Throw NonexistentUniformException if the uniform does not exist
    //   in the program

    void setUniform(const char* name, const GLfloat scalar);
    void setUniform(const char* name, const GLuint scalar);
    void setUniform(const char* name, const GLint scalar);
    void setUniform(const char* name, const vec3& vector);
    void setUniform(const char* name, const mat3& matrix);
    void setUniform(const char* name, const mat4& matrix);

    template <typename T>
    void setUniform(
        const char* arrayName, uint i, const char* memberName,
        const T& value
        );

    #pragma endregion



    #pragma region Exception definitions

    struct CreateException : public RendererException {
        const char* what() const noexcept override { return "glCreateProgram() failed"; }
    };

    struct LinkException : public RendererException {
        LinkException(const char* log = nullptr) : RendererException(log) {}
        const char* what() const noexcept override { return "Unable to link a shader program"; }
    };

    struct NonexistentUniformException : public RendererException {
        NonexistentUniformException(const char* name)
            : RendererException(name) {
        }
        const char* what() const noexcept override {
            return "The requested uniform variable does not exist";
        }
    };

    #pragma endregion



private:

    GLuint m_handle;
    std::map<std::string, GLint> m_uniformLocations;

};
