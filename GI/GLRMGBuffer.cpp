#include "GLRenderer.h"


GLRenderer::GLRMGBuffer::GLRMGBuffer()
	: m_rmiTransform(m_program), m_rmiMaterial(m_program)
{

    GLShader vertex(GL_VERTEX_SHADER, "330", "G-Buffer VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "G-Buffer FragmentShader");

	vertex.addSource("#define ALL_ATTRIBUTES");
    vertex.addSourceFromFile("shaders/vertex.vert");

    fragment.addSourceFromFile("shaders/color.glsl");
    fragment.addSourceFromFile("shaders/lighting.glsl");
    fragment.addSourceFromFile("shaders/shading.glsl");
    fragment.addSourceFromFile("shaders/g-buffer.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();

}


void GLRenderer::GLRMGBuffer::use() {
    m_program.use();
}


Renderer::RMITransform& GLRenderer::GLRMGBuffer::transformInterface() {
    return m_rmiTransform;
}

Renderer::RMIMaterial& GLRenderer::GLRMGBuffer::materialInterface() {
	return m_rmiMaterial;
}
