#pragma once

#include "Model3D.h"
#include <map>
#include "Object3D.h"
#include <vector>

class WavefrontObjects {

public:

    WavefrontObjects(
        const char* basePath, const char* meshFileName
    );

    WavefrontObjects(
        const char* basePath, const char* meshFileName,
        const char* probesFileName, float numProbes = 100.f
    );

    ~WavefrontObjects();

    const std::vector<const Object3D*>& getObjects() const { return m_objects; }

private:

    std::vector<Material*>                              m_materials;
    std::vector<Mesh*>                                  m_meshes;
    std::map<std::string, std::vector<Model3D*>>        m_models;
    std::map<std::string, const Model3D::ProbeSet*>     m_probes;
    std::vector<const Object3D*>                        m_objects;

    void loadModels(
        const char* basePath,
        const char* meshFileName,
        const char* probesFileName
    );

    void loadModels(
        const char* basePath,
        const char* meshFileName
    );

    void loadProbes(const char* basePath, const char* fileName, float probeSubsetSize);

    void createObjects();



};
