#include "GLRenderer.h"

using namespace std;



#ifndef GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT
    #define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF
    #define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
#endif



GLRenderer::GLRenderer(uint width, uint height)
    : Renderer(width, height)
{

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0, 0, 0, 0);
    glEnable(GL_FRAMEBUFFER_SRGB);

}

unique_ptr<Renderer::RMBufferDebug> GLRenderer::createBufferDebugRM() {
    return std::make_unique<GLRMBufferDebug>();
}


unique_ptr<Renderer::RMForward> GLRenderer::createForwardRM() {
    return std::make_unique<GLRMForward>();
}

unique_ptr<Renderer::RMShadowMap> GLRenderer::createShadowMapRM() {
    return std::make_unique<GLRMShadowMap>();
}

unique_ptr<Renderer::RMRSM> GLRenderer::createRSMRM() {
    return std::make_unique<GLRMRSM>();
}

unique_ptr<Renderer::RMGBuffer> GLRenderer::createGBufferRM() {
    return std::make_unique<GLRMGBuffer>();
}

unique_ptr<Renderer::RMDeferred> GLRenderer::createDeferredRM(const GBuffer& gbuffer) {
    return std::make_unique<GLRMDeferred>(gbuffer);
}

unique_ptr<Renderer::RMProbes> GLRenderer::createProbesRM(const RSM& rsm) {
    return std::make_unique<GLRMProbes>(rsm);
}

unique_ptr<Renderer::RMIndirect> GLRenderer::createIndirectRM(
    const ShadingBuffers& shadingBuffers
) {
    return std::make_unique<GLRMIndirect>(shadingBuffers);
}

unique_ptr<Renderer::FrameBuffer> GLRenderer::createFrameBuffer() {
    return std::make_unique<GLFrameBuffer>(m_width, m_height);
}

unique_ptr<Renderer::RenderBuffers> GLRenderer::createRenderBuffers(
    uint width, uint height
) {
    return std::make_unique<GLRenderBuffers>(width, height);
}



unique_ptr<Renderer::RObject3D>
GLRenderer::createRObject3D(const Object3D& object) {
    return std::make_unique<GLObject3D>(object);
}

unique_ptr<Renderer::RLight>
GLRenderer::createRLight(const Light& light, ShadowMap* sm, RSM* rsm) {
    return std::make_unique<GLLight>(light, sm, rsm);
}



void GLRenderer::clearBuffers(uint mask) {
    glClear(mask);
}

void GLRenderer::enableDepthTest(const bool enable){
    if (enable) glEnable(GL_DEPTH_TEST);
    else        glDisable(GL_DEPTH_TEST);
}

void GLRenderer::setPolygonOffset(const float factor, const float units) {
	glPolygonOffset(factor, units);
}

void GLRenderer::enablePolygonOffset(const bool enable) {
	if(enable)  glEnable(GL_POLYGON_OFFSET_FILL);
    else        glDisable(GL_POLYGON_OFFSET_FILL);
}

void GLRenderer::setBlendingMode(BlendingMode mode) {

    switch(mode){
    
    case BlendingMode::NONE:
        glDisable(GL_BLEND);
        break;

    case BlendingMode::ADDITIVE:
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);
        break;

    }

}


unique_ptr<Renderer::Texture> GLRenderer::createTexture(const void* data, uint width, uint height) {

    return make_unique<GLTexture>(data, width, height);

}


GLRenderer::GLTexture::GLTexture(const void* data, uint width, uint height) {
    
    glGenTextures(1, &m_iTexture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_iTexture);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    float aniso = 0.0f;
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);
    
    glGenerateMipmap(GL_TEXTURE_2D);
}


GLRenderer::GLTexture::~GLTexture() {
    glDeleteTextures(1, &m_iTexture);
}


void GLRenderer::GLTexture::bind(GLenum texUnit) const{
    glActiveTexture(texUnit);
    glBindTexture(GL_TEXTURE_2D, m_iTexture);
}
