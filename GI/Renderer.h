#pragma once

#include <list>

#include "Object3D.h"
#include "Light.h"
#include "Camera.h"
#include <map>


using std::unique_ptr;
using std::shared_ptr;


/**
 * Abstract low level renderer
 */
class Renderer{


public:
    //const int 

    enum RenderingPipeline {
        _DEFERRED                   = 2,
        _DIRECT                     = 4,
        _AMBIENT                    = 32,
        _GI_DIFF                    = 8,
        _GI_SPEC                    = 16,
        _BRUTEFORCE                 = 64,
        FORWARD                     = 1,
        FORWARD_AMBIENT             = FORWARD | _AMBIENT,
        DEFERRED_DIRECT             = _DEFERRED | _DIRECT,
        DEFERRED_GI_DIFF            = _DEFERRED | _GI_DIFF,
        DEFERRED_GI_FULL            = DEFERRED_GI_DIFF | _GI_SPEC,
        DEFERRED_DIRECT_GI_DIFF     = DEFERRED_DIRECT | _GI_DIFF,
        DEFERRED_FULL               = DEFERRED_DIRECT_GI_DIFF | _GI_SPEC,
        DEFERRED_BF_GI_DIFF         = _BRUTEFORCE | DEFERRED_GI_DIFF,
        DEFERRED_BF_GI_FULL         = _BRUTEFORCE | DEFERRED_GI_FULL,
        DEFERRED_DIRECT_BF_GI_DIFF  = _BRUTEFORCE | DEFERRED_DIRECT_GI_DIFF,
        DEFERRED_BF_FULL            = _BRUTEFORCE | DEFERRED_FULL,
    };

    explicit Renderer(uint width, uint height) : m_width(width), m_height(height) {}
    virtual ~Renderer();

    /**
     * @param   rsmDim  RSM's resolution accross one dimension
     * @param   smDim   Shadow map's resolution accross one dimension
     */
    void init(uint smDim, uint rsmDim);

    /**
     * Add an object to render
     */
    void add3DObject(const Object3D& object);

    /**
     * Adds a light
     */
    void addLight(const Light& light);

	/**
	 * Attach a camera, which provides viewProjection matrix
	 */
    virtual void setCamera(const Camera& camera);

    void setPipeline(const RenderingPipeline renderingType)
        { m_renderingPipeline = renderingType; }
    
    RenderingPipeline getPipeline()
    {
        return m_renderingPipeline;
    }

    /**
     * Simply renders all 3D objects.
     */
    virtual void render();


protected:

    // Flags for clearBuffers() method
    // TODO: rename?
    static const int DEPTH_BUFFER_BIT = 0x00000100;
    static const int COLOR_BUFFER_BIT = 0x00004000;

    // Enum for setBlendingMode()
    enum BlendingMode{
        NONE,
        ADDITIVE
    };



    class RObject3D;        // abstract representation of 3D objects for the renderer
    class RLight;           // abstract representation of light for the renderer

    class Texture { public: virtual ~Texture() = 0; };
    
    class RenderTarget;     // render target interface
    class RenderTargetRB;   // abstract render target with render buffers
    class RenderBuffers;    // a collection of buffers in the memory for rendering into
    class FrameBuffer;      // abstract primary frame buffer
    class ShadowMap;        // shadow map
    class RSM;              // reflective shadow map
    class GBuffer;          // g-buffer for deferred rendering
    class ShadingBuffers;   // buffers containing shaded light probes

    class RenderMode;       // abstract rendering mode interface
    class RMBufferDebug;    // abstract rendering mode for RenderBuffers debugging
    class RMForward;        // abstract forward rendering mode
    class RMShadowMap;      // abstract shadow map rendering mode
    class RMRSM;            // abstract reflective shadow map rendering mode
    class RMGBuffer;        // abstract g-buffer (for deferred shading) rendering mode
    class RMDeferred;       // abstract deferred shading rendering mode (from G-buffer)
    class RMProbes;         // shading buffer creation mode
    class RMIndirect;       // abstract rendering mode for indirect illumination using shading buffers

    class RMILight;
    class RMITransform;
	class RMIMaterial;

    const uint m_width, m_height;


    #pragma region Abstract methods to implement


    #pragma region RenderMode factory methods

    virtual unique_ptr<RMBufferDebug>
        createBufferDebugRM() = 0;

    virtual unique_ptr<RMForward>
        createForwardRM() = 0;

    virtual unique_ptr<RMShadowMap>
        createShadowMapRM() = 0;

    virtual unique_ptr<RMRSM>
        createRSMRM() = 0;

    virtual unique_ptr<RMGBuffer>
        createGBufferRM() = 0;

    virtual unique_ptr<RMDeferred>
        createDeferredRM(const GBuffer& gbuffer) = 0;

    virtual unique_ptr<RMProbes>
        createProbesRM(const RSM& rsm) = 0;

    virtual unique_ptr<RMIndirect>
        createIndirectRM(const ShadingBuffers& shadingBuffers) = 0;

    #pragma endregion


    #pragma region RenderTarget factory methods

    virtual unique_ptr<FrameBuffer>
        createFrameBuffer() = 0;

    virtual unique_ptr<RenderBuffers>
        createRenderBuffers(uint width, uint height) = 0;
    
    #pragma endregion


    #pragma region Scene Object Wrapper factory methods

    virtual unique_ptr<RObject3D>
        createRObject3D(const Object3D& object) = 0;

    virtual unique_ptr<RLight>
        createRLight(const Light& light, ShadowMap* shadowMap, RSM* rsm) = 0;

    #pragma endregion

	
    #pragma region graphics API abstraction

    virtual unique_ptr<Texture> createTexture(const void* data, uint width, uint height) = 0;

    virtual void clearBuffers(uint mask) = 0;

    virtual void enableDepthTest(const bool enable) = 0;

	virtual void setPolygonOffset(const float factor, const float units) = 0;

	virtual void enablePolygonOffset(const bool enable) = 0;

    virtual void setBlendingMode(BlendingMode mode) = 0;

    #pragma endregion


    #pragma endregion


private:

    // screen filling quad
    class ScreenQuad;
    ScreenQuad*                                 m_screenQuadBase;
    unique_ptr<RObject3D>                       m_screenQuad;
    
    // scene objects
    std::list<unique_ptr<RObject3D>>            m_objects;
    std::list<unique_ptr<RLight>>               m_lights;
    const Camera*                               m_camera = nullptr;

    // key is texture file name
    std::map<std::string, unique_ptr<Texture>>  m_textures;

    // key is material ID, value is texture ID
    std::map<uint,Texture*>                     m_diffuseTextures;
    std::map<uint, Texture*>                    m_specularTextures;

    // render targets
    unique_ptr<FrameBuffer>                     m_frameBuffer;
    ShadowMap*                                  m_shadowMap;
    RSM*                                        m_rsm;
    GBuffer*                                    m_gbuffer;
    ShadingBuffers*                             m_shadingBuffers;

    // render modes
    unique_ptr<RMBufferDebug>                   m_rmBufferDebug;
    unique_ptr<RMForward>                       m_rmForward;
    unique_ptr<RMGBuffer>                       m_rmGBuffer;
    unique_ptr<RMDeferred>                      m_rmDeferred;
    unique_ptr<RMShadowMap>                     m_rmShadowMap;
    unique_ptr<RMRSM>                           m_rmRSM;
    unique_ptr<RMProbes>                        m_rmProbes;
    unique_ptr<RMIndirect>                      m_rmIndirect;

    RenderingPipeline                                   m_renderingPipeline = DEFERRED_FULL;



    void renderForward(const mat4& viewMatrix, const mat4& viewProjMatrix);
    void renderDeferred(const mat4& viewMatrix, const mat4& viewProjMatrix);

    /**
     * Renders all objects in m_objects to a given shadow map.
     *
     * The shadow map remains bound and m_rmShadowMap set for use!
     *
     * @param shadowMap         where to render the objects to
     * @param viewProjMatrix    the matrix to transform the objects
     */
    void renderObjectsToShadowMap(
        ShadowMap& shadowMap,
        const mat4& viewProjMatrix
    );

    void renderObjectsToRSM(
        const mat4& cameraViewMatrix,
        RLight& rLight
    );
    
    /**
     * Renders all objects to a bound render target.
     * lightInterface is used when not null and light not null.
     */
    void renderObjects(
        const mat4& viewMatrix, const mat4& viewProjMatrix,
        RMITransform& transformInterface, RMIMaterial& materialInteface,
        RMILight* lightInterface = nullptr, SpotLight* light = nullptr
    );

    /**
     * "Shades" the probes in the shading buffers.
     */
    void fillShadingBuffers(const Light& light, const mat4& viewMatrix);

    /**
     * Renders indirect light using the shading buffers
     */
    void renderIndirectLight(const mat4& viewMatrix, const mat4& viewProjMatrix);

    /**
     * Draws a fullscreen quad textured by a given RenderBuffers buffer.
     * @param bufferID  ID returned from RenderBuffers::createBuffer()
     *                  or RenderBuffers::getXXXBufferID()
     */
    void debugBuffers(uint bufferID, int x, int y, int width, int height);

    void addMaterialTexture(
        std::map<uint, Texture*>& materialTextureMap, 
        const std::string& texFileName,
        uint materialId
    );

};


#include "SceneObjectsWrappers.h"   // RObject3D and RLight

#include "RenderTargets.h"          // RenderTarget, RenderBuffers,
                                    // RenderTargetRB, FrameBuffer,
                                    // ShadowMap, G-Buffer

#include "RenderModes.h"            // RenderMode, RMForward, RMShadowMap

#include "ScreenQuad.h"
