#include "GLRenderer.h"

using glm::vec4;

GLRenderer::GLRMProbes::GLRMProbes(const RSM& rsm) : RMProbes(rsm), m_rmiLight(m_program) {

    GLShader vertex(GL_VERTEX_SHADER, "330", "Light Probes VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "Light Probes FragmentShader");

    vertex.addSourceFromFile("shaders/probes.glsl");
    vertex.addSourceFromFile("shaders/light_probes.vert");

    fragment.addSourceFromFile("shaders/lighting.glsl");
    fragment.addSourceFromFile("shaders/light_probes.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();

}

void GLRenderer::GLRMProbes::use() {

    m_program.use();

    m_program.setUniform("rsmDim", m_rsm.getRenderBuffers().getWidth());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_rsm.getPositionBufferID());
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_rsm.getNormalBufferID());
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, m_rsm.getFluxBufferID());
    m_program.setUniform("rsmPositionTex", 0);
    m_program.setUniform("rsmNormalTex", 1);
    m_program.setUniform("rsmFluxTex", 2);

}

void GLRenderer::GLRMProbes::enableSpecular(bool enable) {
    m_program.setUniform("enableSpecular", enable);
}

void GLRenderer::GLRMProbes::setShadingBufferDim(uint dim) {
    m_program.setUniform("shadingBufferDim", dim);
}

void GLRenderer::GLRMProbes::setCameraViewMatrix(const mat4& viewMatrix) {
    m_cameraViewMatrix = viewMatrix;
}

void GLRenderer::GLRMProbes::setLight(const Light& light) {
    auto& spot = static_cast<const SpotLight&>(light); // TODO
    m_program.setUniform(
        "light.position",
        vec3(m_cameraViewMatrix*vec4(spot.getPos(), 1.f))
    );
    m_program.setUniform("light.cutoff", spot.getCutoff());
}

void GLRenderer::GLRMProbes::setFirstProbeID(uint firstProbeID) {
    m_program.setUniform("firstProbeID", firstProbeID);
}

void GLRenderer::GLRMProbes::setMatrices(const mat4& modelViewMatrix, const mat3& normalMatrix) {
    m_program.setUniform("modelViewMatrix", modelViewMatrix);
    m_program.setUniform("normalMatrix", normalMatrix);
}

void GLRenderer::GLRMProbes::setMaterialShininess(const float shininess) {
    m_program.setUniform("shininess", shininess);
}