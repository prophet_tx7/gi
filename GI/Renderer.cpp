#include "Renderer.h"

#include "glm.hpp"
#include "gtc/matrix_inverse.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using glm::vec4;


/******************** Public methods ********************/


void Renderer::init(uint smDim, uint rsmDim) {

    m_frameBuffer       = createFrameBuffer();
    m_gbuffer           = new GBuffer(this);
    m_shadowMap         = new ShadowMap(this, smDim);
    m_rsm               = new RSM(this, rsmDim);
    m_shadingBuffers    = new ShadingBuffers(this);
    
    m_rmBufferDebug     = createBufferDebugRM();

    m_rmForward         = createForwardRM();

    m_rmGBuffer         = createGBufferRM();
    m_rmDeferred        = createDeferredRM(*m_gbuffer);

    m_rmShadowMap       = createShadowMapRM();
    m_rmRSM             = createRSMRM();
    m_rmProbes          = createProbesRM(*m_rsm);
    m_rmIndirect        = createIndirectRM(*m_shadingBuffers);

    m_screenQuadBase    = new ScreenQuad();
    m_screenQuad        = createRObject3D(*m_screenQuadBase);

    m_frameBuffer->bind();

}


Renderer::~Renderer() {
    delete m_screenQuadBase;
    delete m_shadowMap;
    delete m_rsm;
    delete m_shadingBuffers;
}


void Renderer::add3DObject(const Object3D& object) {
    
    m_objects.push_back(createRObject3D(object));

    // and now for textures...
    auto& material = object.getModel().getMaterial();
    addMaterialTexture(m_diffuseTextures,  material.diffTexFileName, material.getId());
    addMaterialTexture(m_specularTextures, material.specTexFileName, material.getId());

}


void Renderer::addMaterialTexture(
    std::map<uint, Texture*>& materialTextureMap,
    const std::string& texFileName,
    uint materialId
) {

    if (texFileName.empty()) return;

    if (materialTextureMap.find(materialId) != materialTextureMap.end()) return;

    auto iter = m_textures.find(texFileName);
    if (iter != m_textures.end()) {
        materialTextureMap[materialId] = iter->second.get();
        return;
    }

    int width, height, _;
    auto textureData = stbi_load(texFileName.c_str(), &width, &height, &_, 3);
    m_textures[texFileName] = createTexture(textureData, width, height);
    materialTextureMap[materialId] = m_textures[texFileName].get();
    stbi_image_free(textureData);
}



void Renderer::addLight(const Light& light) {
    m_lights.push_back(createRLight(light, m_shadowMap, m_rsm));
}


void Renderer::setCamera(const Camera& camera) {
    m_camera = &camera;
}


void Renderer::render() {

    // acquire camera matrices
    mat4 viewMatrix, viewProjMatrix;
    if (m_camera) {
        viewMatrix      = m_camera->getViewMatrix();
        viewProjMatrix  = m_camera->getViewProjectionMatrix();
    }

    if (m_renderingPipeline & RenderingPipeline::FORWARD)
        renderForward(viewMatrix, viewProjMatrix);
    else
        renderDeferred(viewMatrix, viewProjMatrix);

}





/******************** Private methods ********************/


void Renderer::renderForward(const mat4& viewMatrix, const mat4& viewProjMatrix) {

    m_frameBuffer->bind();
    clearBuffers(COLOR_BUFFER_BIT);

    // Render the scene for each light
    for (auto& rLight : m_lights) {
		
		auto light = rLight->getLight();
        auto spot = static_cast<const SpotLight&>(rLight->getLight()); // TODO: temp

        // Set the shadow map if there is some
        if (rLight->getShadowMap() && light.getType() == Light::Spot) {
            renderObjectsToShadowMap(
                *rLight->getShadowMap(), spot.getViewProjMatrix()
            );
            m_frameBuffer->bind();
        } else {
            // TODO: allow to turn the shadow map off
            // lightInterface.disableShadowMap();
        }

        m_rmForward->use();
        m_rmForward->enableAmbient((m_renderingPipeline & _AMBIENT) != 0);
		clearBuffers(DEPTH_BUFFER_BIT);
        setBlendingMode(NONE);
        renderObjects(
            viewMatrix, viewProjMatrix,
            m_rmForward->transformInterface(), m_rmForward->materialInterface(),
			&m_rmForward->lightInterface(), &spot
        );
            

    }

}

void Renderer::renderDeferred(const mat4& viewMatrix, const mat4& viewProjMatrix) {

    setBlendingMode(NONE);

    // create g-buffer
    m_gbuffer->bind();
    m_rmGBuffer->use();
	clearBuffers(COLOR_BUFFER_BIT | DEPTH_BUFFER_BIT);
    renderObjects(
		viewMatrix, viewProjMatrix,
		m_rmGBuffer->transformInterface(), m_rmGBuffer->materialInterface()
	);

    // for each light...
    m_frameBuffer->bind();
    m_rmDeferred->use();
    clearBuffers(DEPTH_BUFFER_BIT | COLOR_BUFFER_BIT);
    for (auto& rLight : m_lights) {

		auto& light = rLight->getLight();
		auto spot = static_cast<const SpotLight&>(rLight->getLight()); // TODO: temp
        
		// Set the shadow map if there is some
		if (rLight->getShadowMap()) {
			renderObjectsToShadowMap(*rLight->getShadowMap(), spot.getViewProjMatrix());
            m_frameBuffer->bind();
            m_rmDeferred->use();
		} else {
			// TODO: allow to turn the shadow map off
			// lightInterface.disableShadowMap();
		}

        // Set the RSM
        if ((m_renderingPipeline & _GI_DIFF) || (m_renderingPipeline & _GI_SPEC)) {
            enableDepthTest(true);
            renderObjectsToRSM(viewMatrix, *rLight);
             m_frameBuffer->bind();
            m_rmDeferred->use();
        }


        clearBuffers(DEPTH_BUFFER_BIT);
        enableDepthTest(true);
        setBlendingMode(ADDITIVE);

        // deferred shading
		
        mat4 shadowMatrix;
		shadowMatrix *= 0.5f;
		shadowMatrix[3] = vec4(0.5f, 0.5f, 0.5f, 1.f);
		shadowMatrix = shadowMatrix * spot.getViewProjMatrix() * glm::inverse(viewMatrix);
		m_rmDeferred->lightInterface().setShadowMatrix(shadowMatrix);
		m_rmDeferred->lightInterface().setShadowMap(*rLight->getShadowMap());
        m_rmDeferred->lightInterface().setLight(viewMatrix, light);
        
        m_rmDeferred->enableDirectLighting((m_renderingPipeline & _DIRECT) != 0);
        if (m_renderingPipeline & _BRUTEFORCE) {
            m_rmDeferred->setRSM(m_rsm);
            m_rmDeferred->enableIndirectLighting(
                (m_renderingPipeline & _GI_DIFF) != 0,
                (m_renderingPipeline & _GI_SPEC) != 0
            );
        } else m_rmDeferred->enableIndirectLighting(false, false);
        
         m_screenQuad->drawMesh();

        // indirect light

        if (
            m_renderingPipeline & _BRUTEFORCE
            || !(m_renderingPipeline & _GI_DIFF) && !(m_renderingPipeline & _GI_SPEC)
        ) return;
        if (!rLight->getRSM()) return;

        // shading buffers
        enableDepthTest(false);
        fillShadingBuffers(light, viewMatrix);
        enableDepthTest(true);

        // Draw it
        renderIndirectLight(viewMatrix, viewProjMatrix);

    }

}


void Renderer::renderObjectsToShadowMap(
    ShadowMap& shadowMap,
    const mat4& viewProjMatrix
){

    shadowMap.waitWhileBeingRead();
    shadowMap.bind();
    m_rmShadowMap->use();
	enablePolygonOffset(true);
	setPolygonOffset(2.f, 4.f);
    clearBuffers(DEPTH_BUFFER_BIT);

    for (auto& object : m_objects) {

        // set matrices
        const mat4& modelMatrix = object->getObject3D().getTransform().getMatrix();
        m_rmShadowMap->setMVP(viewProjMatrix*modelMatrix);

        // drawMesh it
        object->drawMesh();

    }

	enablePolygonOffset(false);

}


void Renderer::renderObjectsToRSM(
    const mat4& cameraViewMatrix,
    RLight& rLight
) {

    auto& rsm = *rLight.getRSM(); // assuming the RSM is not null when calling this method

    rsm.waitWhileBeingRead();
    rsm.bind();
    m_rmRSM->use();
    m_rmRSM->setCameraViewMatrix(cameraViewMatrix);
    m_rmRSM->setLight(rLight.getLight());
    clearBuffers(DEPTH_BUFFER_BIT | COLOR_BUFFER_BIT);

    for (auto& object : m_objects) {

        // set matrices
        const mat4& modelMatrix = object->getObject3D().getTransform().getMatrix();
        m_rmRSM->setModelMatrix(modelMatrix);

		// set material
		m_rmRSM->materialInterface().setMaterial(
            object->getObject3D().getModel().getMaterial(),
            m_diffuseTextures
                [object->getObject3D().getModel().getMaterial().getId()],
            nullptr,
            false
		);

        // draw it
        object->drawMesh();

    }

}


void Renderer::renderObjects(
    const mat4& viewMatrix, const mat4& viewProjMatrix,
    RMITransform& transformInterface, RMIMaterial& materialInteface,
    RMILight* lightInterface, SpotLight* light
) {

    if (lightInterface && light) {
        lightInterface->setLight(viewMatrix, *light);
    }
    
    for (auto& object : m_objects) {

        // get appropriate matrices
        const mat4& modelMatrix = object->getObject3D().getTransform()
            .getMatrix();
        const mat4 modelViewMatrix = viewMatrix*modelMatrix;
        const mat3 normalMatrix
            = glm::inverseTranspose(mat3(modelViewMatrix));
        const mat4 mvp = viewProjMatrix*modelMatrix;

        // set matrices and material
        transformInterface.setMatrices(modelViewMatrix, normalMatrix, mvp);
		materialInteface.setMaterial(
            object->getObject3D().getModel().getMaterial(),
            m_diffuseTextures
                [object->getObject3D().getModel().getMaterial().getId()],
            m_specularTextures
                [object->getObject3D().getModel().getMaterial().getId()]
		);

        if (lightInterface && light) {

            mat4 shadowMatrix;
            shadowMatrix *= 0.5f;
            shadowMatrix[3] = vec4(0.5f, 0.5f, 0.5f, 1.f);
            shadowMatrix = shadowMatrix * light->getViewProjMatrix() * modelMatrix;

            lightInterface->setShadowMatrix(shadowMatrix);
            lightInterface->setShadowMap(*m_shadowMap);

        }

        // draw it
        object->drawMesh();
    }

}


void Renderer::fillShadingBuffers(const Light& light, const mat4& viewMatrix)
{
    m_shadingBuffers->bind();
    m_rmProbes->use();
    m_rmProbes->setShadingBufferDim(m_shadingBuffers->getRenderBuffers().getWidth());
    m_rmProbes->setCameraViewMatrix(viewMatrix);
    m_rmProbes->setLight(light);
    m_rmProbes->enableSpecular((m_renderingPipeline & _GI_SPEC) != 0);
    clearBuffers(COLOR_BUFFER_BIT);
    for (auto& object : m_objects) {
        if (!object->hasLightProbes()) continue;
        const mat4& modelMatrix = object->getObject3D().getTransform().getMatrix();
        const mat4 modelViewMatrix = viewMatrix*modelMatrix;
        const mat3 normalMatrix = glm::inverseTranspose(mat3(modelViewMatrix));
        m_rmProbes->setFirstProbeID(object->getProbeIdOffset());
        m_rmProbes->setMatrices(modelViewMatrix, normalMatrix);
        m_rmProbes->setMaterialShininess(object->getObject3D().getModel().getMaterial().shininess);
        object->drawLightProbes();
    }
}


void Renderer::renderIndirectLight(const mat4& viewMatrix, const mat4& viewProjMatrix) {


    m_frameBuffer->bind();
    m_rmIndirect->use();
    m_rmIndirect->enableSpecular((m_renderingPipeline & _GI_SPEC) != 0);
    for (auto& object : m_objects) {

        if (!object->hasLightProbes()) continue;

        // get appropriate matrices
        const mat4& modelMatrix = object->getObject3D().getTransform()
            .getMatrix();
        const mat4 modelViewMatrix = viewMatrix*modelMatrix;
        const mat3 normalMatrix = glm::inverseTranspose(mat3(modelViewMatrix));
        const mat4 mvp = viewProjMatrix*modelMatrix;

        // set matrices and material
        m_rmIndirect->transformInterface().setMatrices(modelViewMatrix, normalMatrix, mvp);
        m_rmIndirect->materialInterface().setMaterial(
            object->getObject3D().getModel().getMaterial(),
            m_diffuseTextures
            [object->getObject3D().getModel().getMaterial().getId()],
            m_specularTextures
            [object->getObject3D().getModel().getMaterial().getId()]
        );

        m_rmIndirect->setFirstProbeID(object->getProbeIdOffset());

        // draw it
        object->drawMesh();

    }
}


void Renderer::debugBuffers(uint bufferID, int x, int y, int width, int height) {
    m_frameBuffer->bind();
    m_frameBuffer->setRenderingViewPort(x, y, width, height);
    m_rmBufferDebug->use();
    m_rmBufferDebug->setBufferID(bufferID);
    m_screenQuad->drawMesh();
    m_frameBuffer->resetRenderingViewPort();
}


/******************** Another classes ********************/


Renderer::RenderTarget::~RenderTarget() {}

Renderer::Texture::~Texture() {}
