#pragma once

#include "Mesh.h"
#include "Material.h"

/**
 * 3D model representation
 * 
 * Currently it only consists of one mesh.
 */
class Model3D{


public:

    /**
     * Set of model's light probes (sparse shading points).
     */
    struct ProbeSet {
        ProbeSet(uint numProbes)
            : probes(std::make_unique<VertexPN[]>(numProbes)), numProbes(numProbes){}

        std::unique_ptr<VertexPN[]> probes;
        uint numProbes;

        /**
         * Reduces the set probes to a random subset of given size.
         * Currently it does not reduce RAM usage.
         * @param probeSubsetSize size of the new subset 0..1
         */
        void reduce(float probeSubsetSize);
    };

    
    /**
     * Constructs a model with given mesh and default material (white)
     */
    explicit Model3D(const Mesh& mesh) : m_mesh(mesh), m_material(defaultMaterial) {}

    /**
     * Constructs a model with given mesh and material
     */
    explicit Model3D(const Mesh& mesh, const Material& material)
        : m_mesh(mesh), m_material(material)
    {}

    virtual ~Model3D() {
        delete[] m_probeIndices;
    }

    /**
     * @return associated mesh
     */
    const Mesh&		getMesh() const		            { return m_mesh; }
    
    /**
     * @return associated light probe array or nullptr if no probes were associated
     */
    const VertexPN* getLightProbes() const          { return m_probes ? m_probes->probes.get() : nullptr; }
    uint getNumProbes() const                       { return m_probes ? m_probes->numProbes : 0; }
    uint getSizeOfLightProbes() const               { return m_probes ? sizeof(VertexPN) * m_probes->numProbes: 0; }
    uint getNumProbesPerVertex() const              { return m_numProbesPerVertex; }

    /**
     * @return  array of n*m indices to light probe array,
     *          where n is number of vertices and m is number of probes per vertex,
     *          first vertex in the mesh gets first m vertices and so on
     */
    const uint*     getLightProbesIndices() const   { return m_probeIndices; }
	
    /**
     * @return associated material
     */
    const Material& getMaterial() const	            { return m_material; }

    /**
     * Sets a set of light probes for the model, which can be later retrived using
     * getLightProbes().
     * Every mesh vertex gets associated appropriate probe subset of of given size.
     */
    void setLightProbes(const ProbeSet& probeSet);



private:

    static const Material   defaultMaterial;

    const Mesh&             m_mesh;
    const Material&         m_material;

    // light probe positions and normals, null if there are no light probes
    const ProbeSet*         m_probes = nullptr;
    const uint              m_numProbesPerVertex = 8; // TODO: make static const
    // indices to m_probes array, size m_numProbes*m_numProbesPerVertex,
    // ordered by vertex order in the mesh
    uint*                   m_probeIndices = nullptr; 

};
