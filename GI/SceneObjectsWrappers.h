#pragma once


/**
 * Abstract representation of 3D objects for use in Renderer
 * implementations
 */
class Renderer::RObject3D {
    
public:

    explicit RObject3D(const Object3D& object) : m_object(object) {

        if (!hasLightProbes()) return;

        static uint nextProbeID = 0;
        m_probeIdOffset = nextProbeID;
        nextProbeID += object.getModel().getNumProbes();

    }

    virtual ~RObject3D() {}

    /**
     * Gets underlying Object3D object.
     */
    const Object3D& getObject3D() const { return m_object; }

    bool hasLightProbes() const {
        return m_object.getModel().getLightProbes() != nullptr;
    }

    uint getNumLightProbes() const { return m_object.getModel().getNumProbes(); }

    uint getProbeIdOffset() const {
        return m_probeIdOffset;
    }

    /**
     * Draws the object
     */
    virtual void drawMesh() = 0;

    /**
     * Draws light probes as points
     */
    virtual void drawLightProbes();

protected:

    const Object3D& m_object;
    uint            m_probeIdOffset = 0;

};

inline void Renderer::RObject3D::drawLightProbes() {}


/**
 * Abstract representation of lights for use in Renderer
 * implementations
 */
class Renderer::RLight {

public:

    RLight(const Light& light, ShadowMap* shadowMap, RSM* rsm)
        : m_light(light), m_shadowMap(shadowMap), m_rsm(rsm) {}
    virtual ~RLight() {}

    /**
     * Gets underlying Light object.
     */
    const Light& getLight() const   { return m_light; }

    /**
     * Gets associated shadow map.
     * May return null.
     */
    ShadowMap* getShadowMap()       { return m_shadowMap; }

    /**
     * Gets associated reflective shadow map.
     * May return null.
     */
    RSM* getRSM()             { return m_rsm; }

protected:

    const Light&    m_light;
    ShadowMap*      m_shadowMap;
    RSM*            m_rsm;

};
