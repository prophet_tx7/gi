#pragma once

#include <vector>

#include "Object3D.h"
#include "Light.h"
#include "Camera.h"

class Scene {

public:

    virtual ~Scene() {}

    virtual void                                update(float deltaTime) {}
    virtual const std::vector<const Object3D*>& getObjects() const = 0;
    virtual const std::vector<const Light*>&    getLights() const = 0;
    virtual const Camera&                       getCamera() const = 0;
    virtual Camera&                             getCamera() = 0;

};
