#pragma once

#include "gl_core_3_3.h"
#include <vector>

#include "Renderer.h"
#include "RendererExceptions.h"

class GLShader {

public:

    /**
     * @param shaderType    one of GL_*_SHADER
     * @param version       what will apear after #version directive
     * @param name          only for debugging purposes
     */
    GLShader(const GLenum shaderType, const char* version, std::string name);

    /**
     * Automatically calls release(), releases the sources
     * from memory if they were not released
     */
    ~GLShader() { releaseSources(); release(); }
   
   /**
    * Adds source code.
    *
    * There must be no #version directive in the source code.
    * '#line 1 x', where x is the source number (given by the order
    * of adding it and starting from 0), will be appended
    * to the source as its first line.
    *
    * The sources will be compiled and released in createAndCompile()
    * if called or released in the destructor otherwise.
    */
    void addSource(const char* source);

    /**
     * Loads source code from a file to the memory.
     *
     * There must be no #version directive in the file.
     * '#line 1 x', where x is the source number (given by the order
     * of adding it and starting from 0), will be appended
     * to the source as its first line.
     *
     * The sources will be compiled and released in createAndCompile()
     * if called or released in the destructor otherwise.
     *
     * @throws GLShader::LoadException
     */
    void addSourceFromFile(const char* fileName);

    /**
     * Calls glCreate(), compiles added sources and releases them.
     *
     * If the shader is already created it just returns.
     *
     * @throws GlShader::CreateException
     * @throws GLShader::CompileException
     */
    void createAndCompile();

    /**
     * Calls glDeleteShader() if a shader was created
     */
    void release();

    /**
     * @return the handle of the shader or 0 when the shader is not created
     */
    GLuint getHandle(){
        return m_handle;
    }
    


public:

    struct CreateException : public RendererException{
        const char* what() const noexcept override { return "glCreateShader() failed"; }
    };

    struct LoadException : public RendererException{
        LoadException(const char* details) : RendererException(details){}
        const char* what() const noexcept override{ return "Shader loading from file failed"; }
    };

    struct CompileException : public RendererException{
        CompileException(const char* log) : RendererException(log){}
        const char* what() const noexcept override{ return "Unable to compile a shader"; }
    };
    


private:

    std::string m_name;
    GLuint m_handle;
    GLenum m_shaderType;
    std::vector<const GLchar*>  m_sources;
    uint m_numSourceFiles;

    /**
     * Checks if the shader was successfully compiled.
     * @throw GLShader::CompileException if the compilation failed
     */
    void verifyCompilationStatus();

    /**
     * Calls delete[] on all items of m_sources and clears it.
     */
    void releaseSources();

};
