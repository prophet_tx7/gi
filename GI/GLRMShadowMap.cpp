#include "GLRenderer.h"

GLRenderer::GLRMShadowMap::GLRMShadowMap() {

    GLShader vertex(GL_VERTEX_SHADER, "330", "ShadowMap VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "ShadowMap FragmentShader");

    vertex.addSourceFromFile("shaders/vertex.vert");

    fragment.addSourceFromFile("shaders/blank.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();

}

void GLRenderer::GLRMShadowMap::use() {
    m_program.use();
}

void GLRenderer::GLRMShadowMap::setMVP(const mat4& mvp) {
    m_program.setUniform("mvp", mvp);
}
