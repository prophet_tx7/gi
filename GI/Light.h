#pragma once

#include "glm.hpp"

using glm::vec3;



/**
 * Abstract light class
 */
class Light {

public:

	enum Type{Spot = 0};

	inline Type         getType() const         { return m_type; }
    inline float        getIntensity() const    { return m_intensity; }
    inline const vec3&  getColor() const        { return m_color; }

    inline void setIntensity(float intensity)   { m_intensity = intensity; }
    inline void setColor(const vec3& color)     { m_color = color; }

protected:

	Type m_type;

	Light(Type type, float intensity, const vec3& color){
		m_type = type;
		m_intensity = intensity;
        m_color = color;
	}

private:

	float   m_intensity;
    vec3    m_color;
    
};



class SpotLight : public Light{

public:

    SpotLight(
        float intensity, const vec3& color,
        const vec3& pos, const vec3& dir, float cutoff,
        const float near, const float far
    ) : Light(Spot, intensity, color),
        m_pos(pos), m_dir(dir), m_cutoff(cutoff), m_near(near), m_far(far)
    {
        setMatrices();
    }

    inline void setPos(const vec3& pos)     { m_pos = pos; setMatrices(); }
    inline void setDir(const vec3& dir)     { m_dir = dir; setMatrices(); }
    inline void setCutoff(float cutoff)     { m_cutoff = cutoff; setMatrices(); }
    inline void setExponent(float exponent) { m_exponent = exponent; }
    inline void setFade(float fade)         { m_fade = fade; }
    
    // Light params getters:
    inline const vec3&  getPos() const      { return m_pos; }
    inline const vec3&  getDir() const      { return m_dir; }
    inline float        getCutoff() const   { return m_cutoff; }
    inline float        getExponent() const { return m_exponent; }
    inline float        getFade() const     { return m_fade; }

    // Light's view getters:
    const mat4& getViewMatrix() const { return m_view; };
    const mat4& getViewProjMatrix() const { return m_viewProj; };

private:

    // light params
    vec3    m_pos;
    vec3    m_dir;
    float   m_cutoff;
    float   m_exponent = 0;
    float   m_fade = 0.8f;

    // light's view
    float   m_near;
    float   m_far;
    mat4    m_view;
    mat4    m_proj;
    mat4    m_viewProj;

    void setMatrices() {
        // TODO: split to different functions?
        m_view = glm::lookAt(m_pos, m_pos+m_dir, vec3(0.f,1.f,0.f));
        m_proj = glm::perspective(2.f*m_cutoff, 1.f, m_near, m_far);
        m_viewProj = m_proj * m_view;
    }

};
