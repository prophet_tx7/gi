#include "GLRenderer.h"

using glm::vec4;



void GLRenderer::GLRMITransform::setMatrices(const mat4& modelViewMatrix, const mat3& normalMatrix, const mat4& mvp) {
    m_program.setUniform("modelViewMatrix", modelViewMatrix);
    m_program.setUniform("normalMatrix", normalMatrix);
    m_program.setUniform("mvp", mvp);
}



// TODO: copy-paste GLRMRSM::setLight()
void GLRenderer::GLRMILight::setLight(const mat4& viewMatrix, const Light& light) {
    
    // set common parameters
    m_program.setUniform("light.type", static_cast<GLint>(light.getType()));
    m_program.setUniform("light.intensity", light.getIntensity());
    m_program.setUniform("light.color", light.getColor());

    // set light-type-specific parameters
    switch (light.getType()) {

    case Light::Spot:
        const SpotLight& spotLight = static_cast<const SpotLight&>(light);
        m_program.setUniform(
            "light.position",
            vec3(viewMatrix*vec4(spotLight.getPos(), 1.f))
            );
        m_program.setUniform(
            "light.direction",
            glm::normalize(
            vec3(viewMatrix*vec4(spotLight.getDir(), 0.f)))
            );
        m_program.setUniform("light.cutoff", spotLight.getCutoff());
        m_program.setUniform("light.spotExponent", spotLight.getExponent());
        m_program.setUniform("light.spotFade", spotLight.getFade());
        break;

    }

}


void GLRenderer::GLRMILight::setShadowMatrix(const mat4& shadowMatrix) {
    m_program.setUniform("shadowMatrix", shadowMatrix);
}


void GLRenderer::GLRMILight::setShadowMap(const ShadowMap& shadowMap) {

    const auto& buffers = static_cast<const GLRenderBuffers&> (
            shadowMap.getRenderBuffers() );
    const auto shadowMapTexture = buffers.getDepthBufferID();
    m_program.setUniform("shadowMap", 3);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
    
}


void GLRenderer::GLRMIMaterial::setMaterial(
    const Material& material,
    const Texture*  diffuse,
    const Texture*  specular,
    const bool      setSpecular
) {

    // diffuse
    if (material.diffTexFileName.empty() || !diffuse) {
        m_program.setUniform("material.useDiffuseMap", 0);
	    m_program.setUniform("material.diffuse", material.diffuseColor);
    } else {
        m_program.setUniform("material.useDiffuseMap", 1);
        m_program.setUniform("diffuseMap", 0);
        static_cast<const GLTexture*>(diffuse)->bind(GL_TEXTURE0);
    }

    if (!setSpecular) return;

    // specular stuff
    m_program.setUniform("material.shininess", material.shininess);
    if (material.specTexFileName.empty() || !specular) {
        m_program.setUniform("material.useSpecularMap", 0);
        m_program.setUniform("material.specular", material.specularColor);
    } else {
        m_program.setUniform("material.useSpecularMap", 1);
        m_program.setUniform("specularMap", 1);
        static_cast<const GLTexture*>(specular)->bind(GL_TEXTURE1);
    }
}
