#pragma once

/**
 * Render target interface.
 * When bound all rendering goes into it.
 */
class Renderer::RenderTarget {

public:

    virtual void bind() = 0;
    virtual ~RenderTarget() = 0;

};


/**
 * A collection of buffers in the memory for rendering into.
 */
class Renderer::RenderBuffers : public RenderTarget {

public:

    /**
     * Buffer (texture) format
     */
    enum BufferFormat{RGB8, RGB32F, RGBA8, RGBA32F};

    RenderBuffers(uint width, uint height)
        : m_width(width), m_height(height) {}
    
    virtual void createDepthBuffer() = 0;

    /**
     * Creates a buffer of given format.
     * @return identifier of created buffer (texture)
     */
    virtual uint createBuffer(BufferFormat textureFormat) = 0;

    virtual uint getDepthBufferID() const = 0;

    uint getWidth() const   { return m_width; }

    uint getHeight() const  { return m_height; }


    /**
     * Pauses CPU thread, if some of the buffers are being read
     */
    virtual void waitWhileBeingRead() const = 0;

protected:

    uint m_width;
    uint m_height;

};


/**
 * Abstract RenderTarget with RenderBuffers.
 */
class Renderer::RenderTargetRB : public RenderTarget {

public:

    explicit RenderTargetRB(Renderer* renderer, uint width, uint height) {
        m_buffers = renderer->createRenderBuffers(width, height);
    }

    virtual ~RenderTargetRB() = 0 {}    // make it abstract without any special
                                        // virtual function
    void bind() override {
        m_buffers->bind();
    }

    const RenderBuffers& getRenderBuffers() const {
        return *m_buffers;
    }

protected:

    unique_ptr<RenderBuffers> m_buffers;

};


/**
 * Abstract primary frame buffer.
 */
class Renderer::FrameBuffer : public RenderTarget {
    
public:

    FrameBuffer(uint width, uint height) : m_width(width), m_height(height) {};

    virtual void setRenderingViewPort(int x, int y, int width, int height, bool clear=true) = 0;
    virtual void resetRenderingViewPort() = 0;

protected:

    const uint m_width, m_height;

};


/**
 * Shadow map.
 */
class Renderer::ShadowMap : public RenderTargetRB {

public:
    
    explicit ShadowMap(Renderer* renderer, uint res) : ShadowMap(renderer, res, res, true) {}

    void waitWhileBeingRead() const { m_buffers->waitWhileBeingRead(); }

protected:

    ShadowMap(Renderer* renderer, int width, int height, const bool createDepthBuffer)
        : RenderTargetRB(renderer, width, height)
    {
        if (createDepthBuffer) m_buffers->createDepthBuffer();
    }

};


/**
 * G-Buffer for deferred shading.
 */
class Renderer::GBuffer : public RenderTargetRB {
    
public:

    explicit GBuffer(Renderer* renderer)
        : RenderTargetRB(renderer, renderer->m_width, renderer->m_height)
    {
        m_buffers->createDepthBuffer(); // TODO: no need to sample
        m_normalBufferID    = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_positionBufferID  = m_buffers->createBuffer(RenderBuffers::RGBA32F);
        m_materialBufferID  = m_buffers->createBuffer(RenderBuffers::RGB32F);
    }

    /**
     * 3xfloat
     */
    uint getNormalBufferID() const {
        return m_normalBufferID;
    }

    /**
     * 4xfloat buffer.
     * XYZ contains fragment position in camera space,
     * W contains fragment depth in screen space.
     */
    uint getPositionBufferID() const {
        return m_positionBufferID;
    }

    /**
     * 3x float buffer
     * X - compressed diffuse color
     * Y - compressed specular color
     * Z - shininess
     */
    uint getMaterialBufferID() const {
        return m_materialBufferID;
    }

private:

    uint m_normalBufferID;
    uint m_positionBufferID;
    uint m_materialBufferID;


};


/**
 * Reflective Shadow Map.
 */
class Renderer::RSM : public ShadowMap {

public:

    explicit RSM(Renderer* renderer, uint res)
        : ShadowMap(renderer, res, res, false)
    {
        m_buffers->createDepthBuffer(); // TODO: no need to sample

        m_iNormals      = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iPositions    = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iFlux         = m_buffers->createBuffer(RenderBuffers::RGB32F);
    }

    uint getNormalBufferID() const      { return m_iNormals; }
    uint getPositionBufferID() const    { return m_iPositions; }
    uint getFluxBufferID() const        { return m_iFlux; }

private:

    uint m_iNormals, m_iPositions, m_iFlux;

};


/**
 * Buffers containing shaded light probes.
 */
class Renderer::ShadingBuffers : public RenderTargetRB {

public:

    explicit ShadingBuffers(Renderer* renderer) : RenderTargetRB(renderer, 256, 256) {
        m_iProbePos     = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iProbeNormal  = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iDiffVplPos   = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iDiffVplFlux  = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iSpecVplPos   = m_buffers->createBuffer(RenderBuffers::RGB32F);
        m_iSpecVplFlux  = m_buffers->createBuffer(RenderBuffers::RGB32F);
    }

    uint getProbePosBufferID() const    { return m_iProbePos; }
    uint getProbeNormalBufferID() const { return m_iProbeNormal; }
    uint getDiffVplPosBufferID() const  { return m_iDiffVplPos; }
    uint getDiffVplFluxBufferID() const { return m_iDiffVplFlux; }
    uint getSpecVplPosBufferID() const  { return m_iSpecVplPos; }
    uint getSpecVplFluxBufferID() const { return m_iSpecVplFlux; }

private:

    uint    m_iProbePos, m_iProbeNormal,
            m_iDiffVplPos, m_iDiffVplFlux,
            m_iSpecVplPos, m_iSpecVplFlux;

};
