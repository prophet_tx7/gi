#include "GLRenderer.h"

#include "gtc/matrix_inverse.hpp"

using glm::vec4;



GLRenderer::GLRMRSM::GLRMRSM() : m_light(nullptr), m_rmiMaterial(m_program) {

    GLShader vertex(GL_VERTEX_SHADER, "330", "RSM VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "RSM FragmentShader");

    vertex.addSource("#define ALL_ATTRIBUTES");
    vertex.addSourceFromFile("shaders/vertex.vert");

    fragment.addSourceFromFile("shaders/lighting.glsl");
    fragment.addSourceFromFile("shaders/shading.glsl");
    fragment.addSourceFromFile("shaders/rsm.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();

}


void GLRenderer::GLRMRSM::use() {
    m_program.use();
}

void GLRenderer::GLRMRSM::setCameraViewMatrix(const mat4& viewMatrix) {
    m_cameraViewMatrix = viewMatrix;
}


void GLRenderer::GLRMRSM::setModelMatrix(const mat4& modelMatrix) {
    mat4 modelViewMatrix = m_cameraViewMatrix * modelMatrix;
    m_program.setUniform("mvp", m_lightViewProjMatrix*modelMatrix);
    m_program.setUniform("modelViewMatrix", modelViewMatrix);
    m_program.setUniform("normalMatrix", glm::inverseTranspose(mat3(modelViewMatrix)));
}


Renderer::RMIMaterial& GLRenderer::GLRMRSM::materialInterface() {
	return m_rmiMaterial;
}


// TODO: copy-paste GLRMRSM::setLight()
void GLRenderer::GLRMRSM::setLight(const Light& light) {

    auto& spot = static_cast<const SpotLight&>(light); // TODO: temp

    m_lightViewProjMatrix = spot.getViewProjMatrix();

    // set common parameters
    m_program.setUniform("light.type", static_cast<GLint>(light.getType()));
    m_program.setUniform("light.intensity", light.getIntensity());
    m_program.setUniform("light.color", light.getColor());

    // TEMP:
    m_lightViewProjMatrix = spot.getViewProjMatrix();
    m_program.setUniform(
        "light.position",
        vec3(m_cameraViewMatrix*vec4(spot.getPos(), 1.f))
        );
    m_program.setUniform(
        "light.direction",
        glm::normalize(
            vec3(m_cameraViewMatrix*vec4(spot.getDir(), 0.f)))
        );
    m_program.setUniform("light.cutoff", spot.getCutoff());
    
}
