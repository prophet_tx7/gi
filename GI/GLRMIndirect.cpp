#include "GLRenderer.h"

GLRenderer::GLRMIndirect::GLRMIndirect(const ShadingBuffers& shadingBuffers)
    : RMIndirect(shadingBuffers), m_rmiTransform(m_program), m_rmiMaterial(m_program)
{
    GLShader vertex(GL_VERTEX_SHADER, "330", "Indirect light VertexShader");
    GLShader fragment(GL_FRAGMENT_SHADER, "330", "Indirect light FragmentShader");

    vertex.addSourceFromFile("shaders/probes.glsl");
    vertex.addSource("#define ALL_ATTRIBUTES");
    vertex.addSource("#define LIGHT_SKIN");
    vertex.addSourceFromFile("shaders/vertex.vert");

    fragment.addSourceFromFile("shaders/lighting.glsl");
    fragment.addSourceFromFile("shaders/shading.glsl");
    fragment.addSourceFromFile("shaders/indirect.frag");

    m_program.attachShader(vertex);
    m_program.attachShader(fragment);

    m_program.link();
}


void GLRenderer::GLRMIndirect::use() {
    
    m_program.use();

    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, m_shadingBuffers.getProbePosBufferID());
    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_2D, m_shadingBuffers.getProbeNormalBufferID());
    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_2D, m_shadingBuffers.getDiffVplPosBufferID());
    glActiveTexture(GL_TEXTURE13);
    glBindTexture(GL_TEXTURE_2D, m_shadingBuffers.getDiffVplFluxBufferID());
    glActiveTexture(GL_TEXTURE14);
    glBindTexture(GL_TEXTURE_2D, m_shadingBuffers.getSpecVplPosBufferID());
    glActiveTexture(GL_TEXTURE15);                   
    glBindTexture(GL_TEXTURE_2D, m_shadingBuffers.getSpecVplFluxBufferID());

    m_program.setUniform("bufferDim", m_shadingBuffers.getRenderBuffers().getWidth());
    
    m_program.setUniform("probePosTex",         10);
    m_program.setUniform("probeNormalTex",      11);
    m_program.setUniform("probeDiffVplPosTex",  12);
    m_program.setUniform("probeDiffVplFluxTex", 13);
    m_program.setUniform("probeSpecVplPosTex",  14);
    m_program.setUniform("probeSpecVplFluxTex", 15);

}

void GLRenderer::GLRMIndirect::enableSpecular(bool enable) {
    m_program.setUniform("enableSpecular", enable);
}

Renderer::RMITransform& GLRenderer::GLRMIndirect::transformInterface() {
    return m_rmiTransform;
}

Renderer::RMIMaterial& GLRenderer::GLRMIndirect::materialInterface() {
    return m_rmiMaterial;
}

void GLRenderer::GLRMIndirect::setFirstProbeID(uint firstProbeId) {
    m_program.setUniform("firstProbeId", firstProbeId);
}