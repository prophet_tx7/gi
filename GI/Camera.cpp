#include "Camera.h"

#include "gtc/matrix_transform.hpp"


Camera::Camera(
	const vec3& eye, const vec3& target, const vec3& up,
	const float aspectRatio,
    const float near, const float far
) {
	m_view = glm::lookAt(eye, target, up);
	m_proj = glm::perspective(3.14f/4.f, aspectRatio, near, far);
    m_viewProj = m_proj * m_view;
}

void Camera::rotateZ(float angle) {
    m_view = glm::rotate(m_view, angle, vec3(0, 0, 1));
    m_viewProj = m_proj * m_view;
}

void Camera::rotateY(float angle) {
    m_view = glm::rotate(mat4(), angle, vec3(0, 1, 0)) * m_view;
    m_viewProj = m_proj * m_view;
}

void Camera::translateForward(float distance) {
    m_view = glm::translate(mat4(), vec3(0, 0, distance)) * m_view;
    m_viewProj = m_proj * m_view;
}

void Camera::translateRight(float distance) {
    m_view = glm::translate(mat4(), vec3(-distance, 0, 0)) * m_view;
    m_viewProj = m_proj * m_view;
}
