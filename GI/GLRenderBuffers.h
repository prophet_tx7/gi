#pragma once

#include <vector>

class GLRenderer::GLRenderBuffers : public RenderBuffers {

public:

    GLRenderBuffers(uint width, uint height);
    ~GLRenderBuffers();

    void createDepthBuffer() override;
    uint createBuffer(BufferFormat textureFormat) override;

    /**
     * Gets index of the depth texture
     */
    uint getDepthBufferID() const override { return m_iDepthTexture; }

    void waitWhileBeingRead() const override { glFlush(); }


    void bind() override;

private:

    GLuint				m_iFBO;
    GLenum              m_iLastTexture;
    GLenum              m_iLastColorAttachment;
	std::vector<GLenum>	m_drawBuffers;
    GLuint				m_iDepthTexture;


    /**
     * The texture remains bound...
     */
    void createTexture(
        GLenum internalFormat, GLenum format, GLenum type,
        GLuint &iTexture
    );

    void setupFBO(GLenum attachment, GLuint iTexture);
	void setupFBO(GLenum attachment, GLuint iTexture, GLenum drawBufferMode);

};
