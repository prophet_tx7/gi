#pragma once

#include <vector>
#include <algorithm>
#include <memory>

/**
 * Set consisting of up to n highest priority items added.
 */
template <typename priority_t, typename data_t>
class PrioritySet {

public:


    /**
     * @param size maximal size of the set
     */
    explicit PrioritySet(size_t size) : m_maxSize(size) {}


    /**
     * Tries to add an item to the set.
     * 
     * The item will be added if the actual size of the set is lower
     * than the specified maximal size.
     * Otherwise a current item with the lowest priority in the set will be replaced
     * if it has lower priroty than the item being added.
     */
    void tryAdd(priority_t priority, data_t data) {

        if (m_items.size() < m_maxSize) {
            m_items.push_back(item_t(priority, data));
        } else {
            auto minItem = std::min_element(m_items.begin(), m_items.end());
            if (priority > minItem->first) {
                minItem->first = priority;
                minItem->second = data;
            }
        }

    }

    /**
     * Creates and returns an array of items of the set.
     */
    std::unique_ptr<data_t[]> getDataArr() {
        auto arr = std::make_unique<data_t[]>(m_items.size());
        auto i = 0;
        for (auto item : m_items) {
            arr[i] = item.second;
            i++;
        }
        return arr;
    }

    /**
     * Removes all elements from the set
     */
    void clear() {
        m_items.clear();
    };

private:
    
    typedef std::pair<priority_t, data_t> item_t;

    const size_t        m_maxSize;
    priority_t          m_minPriority;
    std::vector<item_t> m_items;

};
